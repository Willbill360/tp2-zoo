﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Utilitaires;
using PathFinding;
using Monde;

namespace Humains {
    public class PersonAI : Humain {
        private static readonly Random Rand = new Random();

        public MapPoint TargetPoint { get; set; }
        public MapPoint NextPoint { get; private set; }
        public List<MapPoint> PathToTarget { get; set; }
        public float TimeAlive { get; set; }

        /// <summary>
        /// Constructeur de la classe PersonAI
        /// </summary>
        /// <param name="positionInitiale"></param>
        /// <param name="humain"></param>
        public PersonAI(MapPoint positionInitiale, TuilesHumains humain) : base(positionInitiale, humain) {
            AjouterTuileBanni((int)TuilesCloture.CLOTURE_PORTE);
            TimeAlive = 0;
        }

        /// <summary>
        /// Permet de trouver le prochain point vers où aller
        /// </summary>
        /// <returns>Le prochain point sur lequel s'enligner</returns>
        internal MapPoint NextTarget() {
            NextPoint = PathToTarget[0];
            PathToTarget.Remove(NextPoint);

            return NextPoint;
        }

        /// <summary>
        /// Permet de trouver le prochain objectif a effecter
        /// </summary>
        /// <param name="monde">Le monde dans lequel trouver l'objectif</param>
        internal virtual void FindNextJob(Map monde) {
            MapPoint nextJob;

            if (TimeAlive >= 600) { // Après 10 min le visiteur quitte (sinon il va se ruiner)
                nextJob = monde.ExitPoint;
            } else {
                do {
                    nextJob = new MapPoint(Rand.Next(0, monde.TuilesMonde.GetLength(0)), Rand.Next(0, monde.TuilesMonde.GetLength(1)));
                } while (TuilesBannies.Contains(monde.TuilesMonde[nextJob.X, nextJob.Y]) || TuilesBannies.Contains(monde.TuilesObjets[nextJob.X, nextJob.Y]));
            }
            TargetPoint = nextJob;
        }

        /// <summary>
        /// Permet de trouver un chemin selon l'algorithme A*
        /// </summary>
        /// <param name="monde">Le monde dans lequel trouver le chemin</param>
        /// <param name="walkableTiles">Les tuiles sur lesquelles il est possible de se déplacer</param>
        internal void EvaluatePath(Map monde, bool[,] walkableTiles) {
            if (TargetPoint == null && TargetPoint != monde.ExitPoint) {
                FindNextJob(monde);
            }
            if (TargetPoint == monde.ExitPoint) {
                walkableTiles[monde.ExitPoint.X, monde.ExitPoint.Y] = true;
            }
            PathToTarget = Pathfinding.FindPath(new Grid(monde.TuilesMonde.GetLength(0), monde.TuilesMonde.GetLength(1), walkableTiles), Position, TargetPoint);
            if (PathToTarget.Count > 0) {
                NextTarget();
            } else {
                FindNextJob(monde);
            }
        }

        /// <summary>
        /// Permet de se mettre en marche
        /// </summary>
        /// <param name="monde">Le monde dans lequel se déplacer</param>
        internal virtual void Move(Map monde) {
            if (TargetPoint != null && NextPoint != null) {
                if (PathToTarget.Count >= 0) {
                    Direction direction = ObtenirDirectionMouvement(Position, NextPoint);
                    ModifierOrientation(direction);

                    if (!IsAbleToMove(monde, NextPoint)) {
                        bool[,] fixedWalkableTiles = monde.WalkableTilesAI.Clone() as bool[,];
                        fixedWalkableTiles[NextPoint.X, NextPoint.Y] = false;

                        EvaluatePath(monde, fixedWalkableTiles);
                    }

                    monde.TabHumains[Position.X, Position.Y] = null;
                    Position = NextPoint;
                    monde.TabHumains[NextPoint.X, NextPoint.Y] = this;

                    if (PathToTarget.Count > 0) {
                        NextTarget();
                    } else {
                        TargetPoint = null;
                    }
                } else {
                    TargetPoint = null;
                }
            } else {
                EvaluatePath(monde, monde.WalkableTilesAI);
            }
        }
    }
}
