﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Utilitaires;
using PathFinding;
using Monde;

namespace Humains {
    public enum Sexe {
        HOMME,
        FEMME
    }

    public enum Poses {
        FACE_IDLE,
        DOS_IDLE,
        GAUCHE_IDLE,
        FACE_MARCHE,
        DOS_MARCHE,
        GAUCHE_MARCHE_1,
        GAUCHE_MARCHE_2,
        DROIT_MARCHE_2,
        DROIT_MARCHE_1,
        DROIT_IDLE
    }

    public enum Direction {
        NORD,
        OUEST,
        SUD,
        EST
    }

    public abstract class Humain {
        private static readonly Random Rand = new Random();
        public int DayInsideZoo { get; private set; }
        public string Prenom;
        public string Nom;
        public Sexe Genre;
        public MapPoint Position;
        public Bitmap OrientationCourrante;
        public List<Bitmap> Orientations;

        /// <summary>
        /// Les tuiles bannies (sur lesquelles on ne peut pas marcher)
        /// </summary>
        public List<int> TuilesBannies = new List<int>() {
            (int)TuilesCloture.CLOTURE_DROITE,
            // (int)TuilesCloture.CLOTURE_PORTE,
            (int)TuilesCloture.CLOTURE_SIMPLE,
            (int)TuilesCloture.MUR,
            (int)TuilesBase.BUISSON,
            (int)TuilesMaison.BOITE_FRUIT_ORANGE,
            (int)TuilesMaison.BOITE_FRUIT_POMME,
            (int)TuilesMaison.COTER_MUR_DROIT_BAS,
            (int)TuilesMaison.COTER_MUR_DROIT_HAUT,
            (int)TuilesMaison.COTER_MUR_GAUCHE_BAS,
            (int)TuilesMaison.COTER_MUR_GAUCHE_HAUT,
            (int)TuilesMaison.MUR_MAISON_BAS,
            (int)TuilesMaison.MUR_MAISON_HAUT,
            (int)TuilesMaison.PORTE_HAUT,
            (int)TuilesMaison.TOIT_DROIT_BAS,
            (int)TuilesMaison.TOIT_DROIT_HAUT,
            (int)TuilesMaison.TOIT_DROIT_MILIEU,
            (int)TuilesMaison.TOIT_GAUCHE_BAS,
            (int)TuilesMaison.TOIT_GAUCHE_HAUT,
            (int)TuilesMaison.TOIT_GAUCHE_MILIEU,
            (int)TuilesMaison.TOIT_MILIEU_DROIT_BAS,
            (int)TuilesMaison.TOIT_MILIEU_DROIT_HAUT,
            (int)TuilesMaison.TOIT_MILIEU_DROIT_MILIEU,
            (int)TuilesMaison.TOIT_MILIEU_GAUCHE_BAS,
            (int)TuilesMaison.TOIT_MILIEU_GAUCHE_HAUT,
            (int)TuilesMaison.TOIT_MILIEU_GAUCHE_MILIEU
        };

        /// <summary>
        /// Consctructeur de la classe Humain
        /// </summary>
        /// <param name="positionDepart">La position de départ sur laquel l'humain se trouve</param>
        /// <param name="humain">Le type de "skin" pour l'humain</param>
        public Humain(MapPoint positionDepart, TuilesHumains humain) {
            switch(humain) {
                case TuilesHumains.HUMAIN_1:
                    Genre = Sexe.FEMME;
                    break;
                case TuilesHumains.HUMAIN_2:
                    Genre = Sexe.HOMME;
                    break;
                case TuilesHumains.HUMAIN_3:
                    Genre = Sexe.FEMME;
                    break;
                case TuilesHumains.HUMAIN_4:
                    Genre = Sexe.HOMME;
                    break;
                case TuilesHumains.HUMAIN_5:
                    Genre = Sexe.HOMME;
                    break;
                case TuilesHumains.HUMAIN_6:
                    Genre = Sexe.FEMME;
                    break;
                case TuilesHumains.HUMAIN_7:
                    Genre = Sexe.FEMME;
                    break;
                case TuilesHumains.HUMAIN_8:
                    Genre = Sexe.HOMME;
                    break;
                case TuilesHumains.CONCIERGE:
                    Genre = Sexe.HOMME;
                    break;
                case TuilesHumains.JOUEUR:
                    Genre = Sexe.HOMME;
                    break;
            }
            Prenom = ChoisirPrenom(LoadTextRessource.LoadPrenom(Genre));
            Nom = ChoisirNom(LoadTextRessource.LoadNomFamille());
            DayInsideZoo = 0;
            Position = positionDepart;

            ChargerImages(humain);
        }

        /// <summary>
        /// Permet de récupéré un prénom aléatoirement
        /// </summary>
        /// <param name="listePrenom">La liste de prénom dans laquelle piger</param>
        /// <returns>Un prénom aléatoire</returns>
        private string ChoisirPrenom(List<string> listePrenom) {
            return listePrenom[Rand.Next(0, listePrenom.Count())];
        }

        /// <summary>
        /// Permet de récupéré un nom de famille aléatoirement
        /// </summary>
        /// <param name="listeNomFamille">La liste de nom de famille dans laquelle piger</param>
        /// <returns>Un nom de famille aléatoire</returns>
        private string ChoisirNom(List<string> listeNomFamille) {
            return listeNomFamille[Rand.Next(0, listeNomFamille.Count())];
        }

        /// <summary>
        /// Permet de charger les images de l'humain selon son type
        /// </summary>
        /// <param name="humain">Le type d'humain</param>
        private void ChargerImages(TuilesHumains humain) {
            Orientations = new List<Bitmap>();
            for (int i = (int)humain; i < (int)humain + Enum.GetValues(typeof(Poses)).Length; i++) {
                Orientations.Add(TilesetImageGenerator.GetTile(i));
            }

            OrientationCourrante = Orientations[0]; // FACE_IDLE
        }

        /// <summary>
        /// Permet d'ajouter une tuile à bannir
        /// </summary>
        /// <param name="tuile">La tuile a bannir</param>
        internal void AjouterTuileBanni(int tuile) {
            TuilesBannies.Add(tuile);
        }

        /// <summary>
        /// Permet d'obtenir la direction du mouvement
        /// </summary>
        /// <param name="currentPos">La position courante</param>
        /// <param name="targetPos">La position vers laquel on veux se dirigé</param>
        /// <returns>La direction vers la destination</returns>
        internal static Direction ObtenirDirectionMouvement(MapPoint currentPos, MapPoint targetPos) {
            if(currentPos.X < targetPos.X) {
                return Direction.EST;
            } else if(currentPos.Y < targetPos.Y) {
                return Direction.SUD;
            } else if(currentPos.X > targetPos.X) {
                return Direction.OUEST;
            } else {
                return Direction.NORD;
            }
        }

        /// <summary>
        /// Permet de modifier l'image de l'humain selon la direction
        /// </summary>
        /// <param name="direction">La direction vers laquelle se trouner</param>
        internal void ModifierOrientation(Direction direction) {
            switch (direction) {
                case Direction.NORD:
                    OrientationCourrante = Orientations[(int)Poses.DOS_MARCHE];
                    break;
                case Direction.OUEST:
                    OrientationCourrante = Orientations[(int)Poses.GAUCHE_MARCHE_1];
                    break;
                case Direction.SUD:
                    OrientationCourrante = Orientations[(int)Poses.FACE_MARCHE];
                    break;
                case Direction.EST:
                    OrientationCourrante = Orientations[(int)Poses.DROIT_MARCHE_1];
                    break;
            }
        }

        /// <summary>
        /// Permet de déterminer si l'humain peut bouger sur la position
        /// </summary>
        /// <param name="monde">Le monde dans lequel on tente de bouger</param>
        /// <param name="nextPosition">La position sur laquel on tente de bouger</param>
        /// <returns>SI l'on peu y bouger</returns>
        internal bool IsAbleToMove(Map monde, MapPoint nextPosition) {
            if (TuilesBannies.Contains(monde.TuilesObjets[nextPosition.X, nextPosition.Y])) {
                return false;
            }
            if (TuilesBannies.Contains(monde.TuilesMonde[nextPosition.X, nextPosition.Y])) {
                return false;
            }
            if (monde.TabHumains[nextPosition.X, nextPosition.Y] != null) {
                return false;
            }
            if (monde.TabAnimaux[nextPosition.X, nextPosition.Y] != null) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Ajoute un jour de passé dans le zoo
        /// </summary>
        internal void AjouterJour() {
            DayInsideZoo++;
        }
    }
}
