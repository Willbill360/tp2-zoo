﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Utilitaires;
using PathFinding;
using Monde;

namespace Humains {
    public class Visiteur : PersonAI {
        private static readonly Random Rand = new Random();
        private const int EXIT_CHANCE = 20; // 20%
        private const int TRASH_THROWING_CHANCE = 1; // 1%

        /// <summary>
        /// Constructeur de la classe visiteur
        /// </summary>
        /// <param name="positionInitiale"></param>
        /// <param name="humain"></param>
        public Visiteur(MapPoint positionInitiale, TuilesHumains humain) : base(positionInitiale, humain) {
            AjouterTuileBanni((int)TuilesCloture.CLOTURE_PORTE);
            TimeAlive = 0;
        }

        /// <summary>
        /// Permet de jeter un dechet selon le pourcentage de chance
        /// </summary>
        /// <param name="monde">Le monde dans lequel jeter le dechet</param>
        internal void ThrowTrash(Map monde) {
            if (monde.TuilesDechets[Position.X, Position.Y] == -1) {
                if (Rand.Next(0, 100) < TRASH_THROWING_CHANCE && Position != monde.ExitPoint) {
                    monde.TuilesDechets[Position.X, Position.Y] = Rand.Next((int)TuilesDechet.BOUTEILLE, (int)TuilesDechet.GOBELET + 1);
                    monde.ListeDechets.Enqueue(Position);
                }
            }
        }

        /// <summary>
        /// Permet de trouver le prochain obectif a effectué
        /// </summary>
        /// <param name="monde">Le monde dans lequel trouver l'objectif</param>
        internal override void FindNextJob(Map monde) {
            if (TimeAlive >= 60 && Rand.Next(0, 100) < EXIT_CHANCE) {
                TargetPoint = monde.ExitPoint;
            } else {
                base.FindNextJob(monde);
            }
        }

        /// <summary>
        /// Permet de mettre en marche le visiteur
        /// </summary>
        /// <param name="monde">Le mande dans lequel se déplacé</param>
        internal override void Move(Map monde){
            ThrowTrash(monde);
            base.Move(monde);
        }
    }
}
