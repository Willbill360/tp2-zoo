﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using Utilitaires;
using PathFinding;
using Monde;

namespace Humains {
    public class Joueur : Humain {

        /// <summary>
        /// Constructeur de la classe Joueur
        /// </summary>
        /// <param name="positionInitiale"></param>
        public Joueur(MapPoint positionInitiale) : base(positionInitiale, TuilesHumains.JOUEUR) {
            AjouterTuileBanni((int)TuilesMaison.PORTE_BAS);
        }
        
        /// <summary>
        /// Permet de déplacer le joueur
        /// </summary>
        /// <param name="monde">Le monde dans lequel se déplacer</param>
        /// <param name="direction">La direction dans laquel se déplacer</param>
        internal void Move(Map monde, Direction direction) {
            int hauteurMap = monde.Height;
            int longueurMap = monde.Width;
            MapPoint nextPosition;

            // Remove the player from its last position
            monde.TabHumains[Position.X, Position.Y] = null;

            switch (direction) {
                case Direction.NORD:
                    if (Position.Y > 0) {
                        nextPosition = new MapPoint(Position.X, Position.Y - 1);
                        if (IsAbleToMove(monde, nextPosition)) {
                            Position.Y = Position.Y - 1;
                        }
                        ModifierOrientation(direction);
                    }
                    break;
                case Direction.OUEST:
                    if (Position.X > 0) {
                        nextPosition = new MapPoint(Position.X - 1, Position.Y);
                        if (IsAbleToMove(monde, nextPosition)) {
                            Position.X = Position.X - 1;
                        }
                        ModifierOrientation(direction);
                    }
                    break;
                case Direction.SUD:
                    nextPosition = new MapPoint(Position.X, Position.Y + 1);
                    if (Position.Y < hauteurMap - 1) {
                        if (IsAbleToMove(monde, nextPosition)) {
                            Position.Y = Position.Y + 1;
                        }
                        ModifierOrientation(direction);
                    }
                    break;
                case Direction.EST:
                    nextPosition = new MapPoint(Position.X + 1, Position.Y);
                    if (Position.X < longueurMap - 1) {
                        if(IsAbleToMove(monde, nextPosition)) {
                            Position.X = Position.X + 1;
                        }
                        ModifierOrientation(direction);
                    }
                    break;
            }

            // Place the Monde.Player into its new position
            monde.TabHumains[Position.X, Position.Y] = this;
        }
    }
}
