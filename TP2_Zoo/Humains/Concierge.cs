﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Utilitaires;
using PathFinding;
using Monde;

namespace Humains {
    public class Concierge : PersonAI {
        MapPoint PositionDechetCourant;

        /// <summary>
        /// Constructeur de la classe Concierge
        /// </summary>
        /// <param name="positionInitiale">La position de départ (spawnpoint)</param>
        public Concierge(MapPoint positionInitiale) : base(positionInitiale, TuilesHumains.CONCIERGE) {
            AjouterTuileBanni((int)TuilesCloture.CLOTURE_PORTE);
            PositionDechetCourant = null;
        }

        /// <summary>
        /// Permet de trouver un nouvel objectif
        /// </summary>
        /// <param name="monde">Le monde dans lequel trouver le nouvel objectif</param>
        internal override void FindNextJob(Map monde) {
            if(PositionDechetCourant == Position) {
                monde.TuilesDechets[Position.X, Position.Y] = -1;
                PositionDechetCourant = null;
            }
            if (monde.ListeDechets.Count > 0) {
                TargetPoint = monde.ListeDechets.Dequeue();
                PositionDechetCourant = TargetPoint;
            } else {
                base.FindNextJob(monde);
            }
        }

        /// <summary>
        /// Permet de se mettre en mouvement
        /// </summary>
        /// <param name="monde">Le monde dans lequel se mettre en mouvement</param>
        internal override void Move(Map monde) {
            if (PositionDechetCourant == null && monde.ListeDechets.Count > 0) {
                FindNextJob(monde);
                EvaluatePath(monde, monde.WalkableTilesAI);
            }

            base.Move(monde);
        }
    }
}
