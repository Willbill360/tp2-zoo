﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utilitaires;
using Animaux;
using Monde;

namespace TP2_Zoo {
    public partial class FrmMagasin : Form {
        private MondeJeu jeu;
        public string AnimalChoisi { get; set; }
        public double Montant { get; set; }

        public FrmMagasin(MondeJeu UCJeu) {
            jeu = UCJeu;
            InitializeComponent();
        }
        
        private void FrmMagasin_Load(object sender, EventArgs e) {
            PicLicorne.Image = TilesetImageGenerator.GetTile((int)TuilesAnimal.LICORNE_FACE);
            PicLion.Image = TilesetImageGenerator.GetTile((int)TuilesAnimal.LION_FACE);
            PicRhino.Image = TilesetImageGenerator.GetTile((int)TuilesAnimal.RHINO_FACE);
            PicMouton.Image = TilesetImageGenerator.GetTile((int)TuilesAnimal.MOUTON_FACE);
        }

        private void BtnAchat_Click(object sender, EventArgs e) {
            string type = ((Button)sender).Text;

            this.AnimalChoisi = type;

            switch (type) {
                case "Licorne":
                    this.Montant = (double)PrixAnimaux.LICORNE;
                    break;
                case "Lion":
                    this.Montant = (double)PrixAnimaux.LION;
                    break;
                case "Rhinocéros":
                    this.Montant = (double)PrixAnimaux.RHINO;
                    break;
                case "Mouton":
                    this.Montant = (double)PrixAnimaux.MOUTON;
                    break;
            }
            this.Close();

        }


    }
}
