﻿namespace TP2_Zoo {
    partial class InformationsHumain {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.LblJours = new System.Windows.Forms.Label();
            this.LblTemps = new System.Windows.Forms.Label();
            this.LblGenre = new System.Windows.Forms.Label();
            this.LblNomComplet = new System.Windows.Forms.Label();
            this.PicVisiteur = new System.Windows.Forms.PictureBox();
            this.BtnFermer = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicVisiteur)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.NavajoWhite;
            this.panel1.Controls.Add(this.LblJours);
            this.panel1.Controls.Add(this.PicVisiteur);
            this.panel1.Controls.Add(this.LblTemps);
            this.panel1.Controls.Add(this.LblGenre);
            this.panel1.Controls.Add(this.LblNomComplet);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(255, 116);
            this.panel1.TabIndex = 0;
            // 
            // LblJours
            // 
            this.LblJours.AutoSize = true;
            this.LblJours.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblJours.Location = new System.Drawing.Point(164, 83);
            this.LblJours.Name = "LblJours";
            this.LblJours.Size = new System.Drawing.Size(54, 23);
            this.LblJours.TabIndex = 3;
            this.LblJours.Text = "Jours";
            // 
            // LblTemps
            // 
            this.LblTemps.AutoSize = true;
            this.LblTemps.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTemps.Location = new System.Drawing.Point(15, 83);
            this.LblTemps.Name = "LblTemps";
            this.LblTemps.Size = new System.Drawing.Size(151, 23);
            this.LblTemps.TabIndex = 2;
            this.LblTemps.Text = "Nombre de jours :";
            // 
            // LblGenre
            // 
            this.LblGenre.AutoSize = true;
            this.LblGenre.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGenre.Location = new System.Drawing.Point(94, 48);
            this.LblGenre.Name = "LblGenre";
            this.LblGenre.Size = new System.Drawing.Size(58, 23);
            this.LblGenre.TabIndex = 1;
            this.LblGenre.Text = "Genre";
            // 
            // LblNomComplet
            // 
            this.LblNomComplet.AutoSize = true;
            this.LblNomComplet.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNomComplet.Location = new System.Drawing.Point(94, 12);
            this.LblNomComplet.Name = "LblNomComplet";
            this.LblNomComplet.Size = new System.Drawing.Size(112, 23);
            this.LblNomComplet.TabIndex = 0;
            this.LblNomComplet.Text = "Prenom Nom";
            // 
            // PicVisiteur
            // 
            this.PicVisiteur.Location = new System.Drawing.Point(19, 12);
            this.PicVisiteur.Name = "PicVisiteur";
            this.PicVisiteur.Size = new System.Drawing.Size(64, 64);
            this.PicVisiteur.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicVisiteur.TabIndex = 1;
            this.PicVisiteur.TabStop = false;
            // 
            // BtnFermer
            // 
            this.BtnFermer.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFermer.Location = new System.Drawing.Point(70, 121);
            this.BtnFermer.Name = "BtnFermer";
            this.BtnFermer.Size = new System.Drawing.Size(123, 36);
            this.BtnFermer.TabIndex = 8;
            this.BtnFermer.Text = "Fermer";
            this.BtnFermer.UseVisualStyleBackColor = true;
            this.BtnFermer.Click += new System.EventHandler(this.BtnFermer_Click);
            // 
            // InformationsHumain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.Controls.Add(this.BtnFermer);
            this.Controls.Add(this.panel1);
            this.Name = "InformationsHumain";
            this.Size = new System.Drawing.Size(255, 168);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicVisiteur)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LblNomComplet;
        private System.Windows.Forms.PictureBox PicVisiteur;
        private System.Windows.Forms.Label LblJours;
        private System.Windows.Forms.Label LblTemps;
        private System.Windows.Forms.Label LblGenre;
        private System.Windows.Forms.Button BtnFermer;
    }
}
