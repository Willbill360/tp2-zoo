﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PathFinding;
using Utilitaires;
using Monde;

namespace Animaux {
    public enum Sexe {
        MALE,
        FEMELLE
    }

    public enum PosesAnimal {
        FACE_IDLE,
        DOS_IDLE
    }

    public enum DirectionAnimal {
        NORD,
        OUEST,
        SUD,
        EST
    }

    public enum PrixAnimaux {
        LICORNE = 50,
        LION = 35,
        RHINO = 40,
        MOUTON = 20
    }

    public enum Gestation {
        LICORNE = 360,
        LION = 110,
        RHINO = 480,
        MOUTON = 150
    }

    public abstract class Animal {
        private static readonly Random Rand = new Random();
        private const int PREGANT_CHANCE = 20;

        public MapPoint TargetPoint { get; set; }
        public MapPoint NextPoint { get; private set; }
        public List<MapPoint> PathToTarget { get; set; }
        public float TimeAlive { get; set; }

        public TuilesAnimal ImageAnimal { get; private set; }
        public Sexe Genre { get; private set; }
        public bool IsPregnant { get; private set; }
        public int GestationPriode { get; private set; }
        public int GestatingTime { get; private set; }
        public string Faim { get; set; }
        public int JourFaim { get; set; }

        public Enclo AssignedEnclo { get; private set; }
        public MapPoint Position { get; private set; }
        public Bitmap OrientationCourrante { get; private set; }
        public List<Bitmap> Orientations { get; private set; }

        public List<int> TuilesBannies = new List<int>() {
            (int)TuilesCloture.CLOTURE_DROITE,
            (int)TuilesCloture.CLOTURE_PORTE,
            (int)TuilesCloture.CLOTURE_SIMPLE
        };
        
        /// <summary>
        /// constructeur
        /// </summary>
        /// <param name="positionDepart"></param>
        /// <param name="animal"></param>
        /// <param name="assignedEnclo"></param>
        /// <param name="gestationPeriode"></param>
        public Animal(MapPoint positionDepart, TuilesAnimal animal, Enclo assignedEnclo, int gestationPeriode) {
            ImageAnimal = animal;
            AssignedEnclo = assignedEnclo;
            Genre = (Sexe)Rand.Next(0, 2);
            Faim = "Non";
            IsPregnant = false;
            Position = positionDepart;
            GestationPriode = gestationPeriode;
            ChargerImages(animal);
        }

        /// <summary>
        /// Permet de charger les images selon les orientations
        /// </summary>
        /// <param name="tuile"></param>
        private void ChargerImages(TuilesAnimal tuile) {
            Orientations = new List<Bitmap>();
            for (int i = (int)tuile; i < (int)tuile + Enum.GetValues(typeof(PosesAnimal)).Length; i++) {
                Orientations.Add(TilesetImageGenerator.GetTile(i));
            }

            OrientationCourrante = Orientations[0]; // FACE_IDLE
        }

        /// <summary>
        /// Permet d'obtenir la direction dans laquelle l'animal se deplace
        /// </summary>
        /// <param name="currentPos"></param>
        /// <param name="targetPos"></param>
        /// <returns></returns>
        internal static DirectionAnimal ObtenirDirectionMouvement(MapPoint currentPos, MapPoint targetPos) {
            if (currentPos.X < targetPos.X) {
                return DirectionAnimal.EST;
            } else if (currentPos.Y < targetPos.Y) {
                return DirectionAnimal.SUD;
            } else if (currentPos.X > targetPos.X) {
                return DirectionAnimal.OUEST;
            } else {
                return DirectionAnimal.NORD;
            }
        }

        /// <summary>
        /// Permet de modifier l'orientation de l'animal selon sa direction
        /// </summary>
        /// <param name="direction"></param>
        internal void ModifierOrientation(DirectionAnimal direction) {
            switch (direction) {
                case DirectionAnimal.NORD:
                    OrientationCourrante = Orientations[(int)PosesAnimal.DOS_IDLE];
                    break;
                case DirectionAnimal.OUEST:
                    OrientationCourrante = Orientations[(int)PosesAnimal.FACE_IDLE];
                    break;
                case DirectionAnimal.SUD:
                    OrientationCourrante = Orientations[(int)PosesAnimal.FACE_IDLE];
                    break;
                case DirectionAnimal.EST:
                    OrientationCourrante = Orientations[(int)PosesAnimal.FACE_IDLE];
                    break;
            }
        }

        /// <summary>
        /// Permet de savoir les tuiles qui lui sont interdites
        /// </summary>
        /// <param name="monde"></param>
        /// <param name="nextPosition"></param>
        /// <returns></returns>
        internal bool IsAbleToMove(Map monde, MapPoint nextPosition) {
            if (TuilesBannies.Contains(monde.TuilesObjets[nextPosition.X, nextPosition.Y])) {
                return false;
            }
            if (monde.TabHumains[nextPosition.X, nextPosition.Y] != null) {
                return false;
            }
            if (monde.TabAnimaux[nextPosition.X, nextPosition.Y] != null) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Permet de déterminer si l'animal (femelle) est enceinte
        /// </summary>
        public void DeterminerPossibiliteEnceinte() {
            bool hasMaleInEnclo = false;
            bool hasFemaleInEnclo = false;

            foreach (Animal animal in AssignedEnclo.ListeAnimaux) {
                if (!hasMaleInEnclo && animal.Genre == Sexe.MALE) {
                    hasMaleInEnclo = true;
                    continue;
                }
                if (!hasFemaleInEnclo && animal.Genre == Sexe.FEMELLE && !animal.IsPregnant) {
                    hasFemaleInEnclo = true;
                    continue;
                }
                if(hasMaleInEnclo && hasFemaleInEnclo) {
                    break;
                }
            }

            if (hasMaleInEnclo && hasFemaleInEnclo && AssignedEnclo.ListeAnimaux.Count < 10) {
                IsPregnant = Rand.Next(100) < PREGANT_CHANCE;
            }
        }

        /// <summary>
        /// Permet d'avancer les jours dans la grossesse
        /// </summary>
        internal void AjouterJour() {
            if(IsPregnant) {
                GestatingTime++;
            }
        }

        /// <summary>
        /// Prochain endroit ou se diriger
        /// </summary>
        /// <returns></returns>
        internal MapPoint NextTarget() {
            NextPoint = PathToTarget[0];
            PathToTarget.Remove(NextPoint);

            return NextPoint;
        }

        /// <summary>
        /// Choisir un nouveau chemin jusqu'a temps que cela soit possible
        /// </summary>
        /// <param name="monde"></param>
        internal virtual void FindNextJob(Map monde) {
            MapPoint nextJob;
            List<MapPoint> listeTuileDispo = AssignedEnclo.PositionInterieur;
            do {
                do {
                    do {
                        nextJob = new MapPoint(Rand.Next(0, monde.TuilesMonde.GetLength(0)), Rand.Next(0, monde.TuilesMonde.GetLength(1)));
                    } while (TuilesBannies.Contains(monde.TuilesMonde[nextJob.X, nextJob.Y]) || TuilesBannies.Contains(monde.TuilesObjets[nextJob.X, nextJob.Y]));
                } while (!listeTuileDispo.Contains(nextJob));
            } while (monde.TabAnimaux[nextJob.X, nextJob.Y] != null);

            TargetPoint = nextJob;
        }

        /// <summary>
        /// Permet d'evaluer le chemin a prendre
        /// </summary>
        /// <param name="monde"></param>
        /// <param name="walkableTilesAnimal"></param>
        internal void EvaluatePath(Map monde, bool[,] walkableTilesAnimal) {
            if (TargetPoint == null) {
                FindNextJob(monde);
            }
            PathToTarget = Pathfinding.FindPath(new Grid(monde.TuilesMonde.GetLength(0), monde.TuilesMonde.GetLength(1), walkableTilesAnimal), Position, TargetPoint);
            if (PathToTarget.Count > 0) {
                NextTarget();
            } else {
                FindNextJob(monde);
            }
        }

        /// <summary>
        /// Deplace l'intelligence artificielle
        /// </summary>
        /// <param name="monde"></param>
        internal virtual void Move(Map monde) {
            if (TargetPoint != null && NextPoint != null) {
                if (PathToTarget.Count >= 0) {
                    DirectionAnimal direction = ObtenirDirectionMouvement(Position, NextPoint);
                    ModifierOrientation(direction);

                    if (!IsAbleToMove(monde, NextPoint)) {
                        bool[,] fixedWalkableTiles = AssignedEnclo.WalkableTilesAI(monde).Clone() as bool[,];
                        fixedWalkableTiles[NextPoint.X, NextPoint.Y] = false;

                        EvaluatePath(monde, fixedWalkableTiles);
                    }

                    monde.TabAnimaux[Position.X, Position.Y] = null;
                    Position = NextPoint;
                    monde.TabAnimaux[NextPoint.X, NextPoint.Y] = this;

                    if (PathToTarget.Count > 0) {
                        NextTarget();
                    } else {
                        TargetPoint = null;
                    }
                } else {
                    TargetPoint = null;
                }
            } else {
                EvaluatePath(monde, AssignedEnclo.WalkableTilesAI(monde));
            }
        }

        /// <summary>
        /// Permet à l'animal d'accoucher
        /// </summary>
        /// <param name="monde"></param>
        internal void Accouche(Map monde) {
            AssignedEnclo.AjouterAnimal(monde, GetType().Name);
            GestatingTime = 0;
            IsPregnant = false;
        }
    }
}
