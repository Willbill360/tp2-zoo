﻿namespace TP2_Zoo {
    partial class InformationAnimal {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.PicAnimal = new System.Windows.Forms.PictureBox();
            this.LblRaceAnimal = new System.Windows.Forms.Label();
            this.LblGenre = new System.Windows.Forms.Label();
            this.LblAge = new System.Windows.Forms.Label();
            this.LblFaim = new System.Windows.Forms.Label();
            this.LblFaimOuiNon = new System.Windows.Forms.Label();
            this.LblEnceinte = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BtnFermer = new System.Windows.Forms.Button();
            this.LblSexe = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicAnimal)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // PicAnimal
            // 
            this.PicAnimal.Location = new System.Drawing.Point(14, 10);
            this.PicAnimal.Name = "PicAnimal";
            this.PicAnimal.Size = new System.Drawing.Size(64, 64);
            this.PicAnimal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicAnimal.TabIndex = 0;
            this.PicAnimal.TabStop = false;
            // 
            // LblRaceAnimal
            // 
            this.LblRaceAnimal.AutoSize = true;
            this.LblRaceAnimal.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRaceAnimal.Location = new System.Drawing.Point(117, 34);
            this.LblRaceAnimal.Name = "LblRaceAnimal";
            this.LblRaceAnimal.Size = new System.Drawing.Size(99, 23);
            this.LblRaceAnimal.TabIndex = 2;
            this.LblRaceAnimal.Text = "Rhinoceros";
            // 
            // LblGenre
            // 
            this.LblGenre.AutoSize = true;
            this.LblGenre.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGenre.Location = new System.Drawing.Point(118, 99);
            this.LblGenre.Name = "LblGenre";
            this.LblGenre.Size = new System.Drawing.Size(67, 23);
            this.LblGenre.TabIndex = 3;
            this.LblGenre.Text = "Femelle";
            // 
            // LblAge
            // 
            this.LblAge.AutoSize = true;
            this.LblAge.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAge.Location = new System.Drawing.Point(120, 137);
            this.LblAge.Name = "LblAge";
            this.LblAge.Size = new System.Drawing.Size(65, 23);
            this.LblAge.TabIndex = 4;
            this.LblAge.Text = "Adulte";
            // 
            // LblFaim
            // 
            this.LblFaim.AutoSize = true;
            this.LblFaim.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFaim.Location = new System.Drawing.Point(19, 10);
            this.LblFaim.Name = "LblFaim";
            this.LblFaim.Size = new System.Drawing.Size(55, 23);
            this.LblFaim.TabIndex = 5;
            this.LblFaim.Text = "Faim :";
            // 
            // LblFaimOuiNon
            // 
            this.LblFaimOuiNon.AutoSize = true;
            this.LblFaimOuiNon.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFaimOuiNon.Location = new System.Drawing.Point(84, 9);
            this.LblFaimOuiNon.Name = "LblFaimOuiNon";
            this.LblFaimOuiNon.Size = new System.Drawing.Size(149, 23);
            this.LblFaimOuiNon.TabIndex = 6;
            this.LblFaimOuiNon.Text = "Depuis 16 minutes";
            // 
            // LblEnceinte
            // 
            this.LblEnceinte.AutoSize = true;
            this.LblEnceinte.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEnceinte.Location = new System.Drawing.Point(19, 171);
            this.LblEnceinte.Name = "LblEnceinte";
            this.LblEnceinte.Size = new System.Drawing.Size(81, 23);
            this.LblEnceinte.TabIndex = 8;
            this.LblEnceinte.Text = "Enceinte";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Tan;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.LblRaceAnimal);
            this.panel1.Controls.Add(this.PicAnimal);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(246, 88);
            this.panel1.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Tan;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.BtnFermer);
            this.panel2.Controls.Add(this.LblFaim);
            this.panel2.Controls.Add(this.LblFaimOuiNon);
            this.panel2.Location = new System.Drawing.Point(0, 203);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(246, 88);
            this.panel2.TabIndex = 10;
            // 
            // BtnFermer
            // 
            this.BtnFermer.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFermer.Location = new System.Drawing.Point(61, 41);
            this.BtnFermer.Name = "BtnFermer";
            this.BtnFermer.Size = new System.Drawing.Size(123, 36);
            this.BtnFermer.TabIndex = 7;
            this.BtnFermer.Text = "Fermer";
            this.BtnFermer.UseVisualStyleBackColor = true;
            this.BtnFermer.Click += new System.EventHandler(this.BtnFermer_Click);
            // 
            // LblSexe
            // 
            this.LblSexe.AutoSize = true;
            this.LblSexe.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSexe.Location = new System.Drawing.Point(19, 98);
            this.LblSexe.Name = "LblSexe";
            this.LblSexe.Size = new System.Drawing.Size(68, 23);
            this.LblSexe.TabIndex = 3;
            this.LblSexe.Text = "Genre: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 23);
            this.label1.TabIndex = 11;
            this.label1.Text = "Âge : ";
            // 
            // InformationAnimal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.NavajoWhite;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LblSexe);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LblEnceinte);
            this.Controls.Add(this.LblAge);
            this.Controls.Add(this.LblGenre);
            this.Name = "InformationAnimal";
            this.Size = new System.Drawing.Size(245, 291);
            ((System.ComponentModel.ISupportInitialize)(this.PicAnimal)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PicAnimal;
        private System.Windows.Forms.Label LblRaceAnimal;
        private System.Windows.Forms.Label LblGenre;
        private System.Windows.Forms.Label LblAge;
        private System.Windows.Forms.Label LblFaim;
        private System.Windows.Forms.Label LblFaimOuiNon;
        private System.Windows.Forms.Label LblEnceinte;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label LblSexe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnFermer;
    }
}
