﻿using PathFinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaires;
using Monde;

namespace Animaux {
    public class Mouton : Animal {

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="positionInitiale"></param>
        /// <param name="enclo"></param>
        public Mouton(MapPoint positionInitiale, Enclo enclo) : base(positionInitiale, TuilesAnimal.MOUTON_FACE, enclo, 150) {

        }
    }
}
