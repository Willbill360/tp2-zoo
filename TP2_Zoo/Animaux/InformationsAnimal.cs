﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Animaux;
using Utilitaires;

namespace TP2_Zoo {
    public partial class InformationAnimal : UserControl {
        public InformationAnimal() {
            InitializeComponent();
        }

        /// <summary>
        /// Permet de remplir les informations sur chacun de animaux
        /// </summary>
        /// <param name="animal"></param>
        /// <param name="liste"></param>
        public void RemplirInformationsAnimal(Animal animal, List<Animal> liste) {
            string genre = animal.Genre.ToString().ToLower();
            genre = genre.Substring(0, 1).ToUpper() + genre.Substring(1);
            PicAnimal.Image = TilesetImageGenerator.GetTile((int)animal.ImageAnimal);
            LblRaceAnimal.Text = animal.GetType().Name;
            LblGenre.Text = genre;

            if (animal.Faim.Equals("Non")) {
                LblFaimOuiNon.Text = animal.Faim;
            }
            else {
                LblFaimOuiNon.Text = animal.Faim +", depuis " + animal.JourFaim + " jours" ;
            }
            

            if (animal.Genre.ToString().Equals("MALE")) {
                LblEnceinte.Hide();
            }
            else {
                if (animal.IsPregnant) {
                    LblEnceinte.Show();
                }
                else {
                    animal.DeterminerPossibiliteEnceinte();
                    if (animal.IsPregnant) {
                        LblEnceinte.Show();
                    }
                    else {
                        LblEnceinte.Hide();
                    }
                }
            }
        }

        /// <summary>
        /// Pour permettre a l'utilisateur de fermer la fenetre de facon efficace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnFermer_Click(object sender, EventArgs e) {
            this.Parent.Controls.Remove(this);
        }
    }
}
