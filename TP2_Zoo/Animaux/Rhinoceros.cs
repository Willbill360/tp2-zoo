﻿using PathFinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaires;
using Monde;

namespace Animaux {
    public class Rhinoceros : Animal {

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="positionInitiale"></param>
        /// <param name="enclo"></param>
        public Rhinoceros(MapPoint positionInitiale, Enclo enclo) : base(positionInitiale, TuilesAnimal.RHINO_FACE, enclo, 480) {

        }
    }
}
