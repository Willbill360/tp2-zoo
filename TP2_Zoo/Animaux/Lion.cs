﻿using PathFinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaires;
using Monde;

namespace Animaux {
    public class Lion : Animal {

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="positionInitiale"></param>
        /// <param name="enclo"></param>
        public Lion(MapPoint positionInitiale, Enclo enclo) : base(positionInitiale, TuilesAnimal.LION_FACE, enclo, 110) {

        }
    }
}
