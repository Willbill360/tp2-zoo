﻿using PathFinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilitaires;
using Animaux;
using Monde;

namespace Animaux {
    public class Licorne : Animal {

        /// <summary>
        /// constructeur
        /// </summary>
        /// <param name="positionInitiale"></param>
        /// <param name="enclo"></param>
        public Licorne(MapPoint positionInitiale, Enclo enclo) : base(positionInitiale, TuilesAnimal.LICORNE_FACE, enclo, 360) {

        }
    }
}
