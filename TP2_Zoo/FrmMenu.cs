﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP2_Zoo {
    public partial class FrmMenu : Form {
        public FrmMenu() {
            InitializeComponent();
        }

        private void BtnStart_Click(object sender, EventArgs e) {
            this.Hide();
            FrmZoo zoo = new FrmZoo();
            zoo.ShowDialog();
            this.Close();
        }

        private void BtnQuitter_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
