﻿namespace Monde
{
    partial class MondeJeu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MondeJeu));
            this.PnlTemps = new System.Windows.Forms.Panel();
            this.LblJour = new System.Windows.Forms.Label();
            this.PnlNb = new System.Windows.Forms.Panel();
            this.LblTexteDechets = new System.Windows.Forms.Label();
            this.LblNbDechets = new System.Windows.Forms.Label();
            this.LblArgent = new System.Windows.Forms.Label();
            this.LblNbAnimaux = new System.Windows.Forms.Label();
            this.LblTexteAnimaux = new System.Windows.Forms.Label();
            this.BtnAgence = new System.Windows.Forms.Button();
            this.PicArgent = new System.Windows.Forms.PictureBox();
            this.PnlInfo = new System.Windows.Forms.Panel();
            this.LblTitreBoite = new System.Windows.Forms.Label();
            this.TmrTemps = new System.Windows.Forms.Timer(this.components);
            this.TmrRealTime = new System.Windows.Forms.Timer(this.components);
            this.PnlTemps.SuspendLayout();
            this.PnlNb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicArgent)).BeginInit();
            this.PnlInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // PnlTemps
            // 
            this.PnlTemps.BackColor = System.Drawing.Color.Tan;
            this.PnlTemps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlTemps.Controls.Add(this.LblJour);
            this.PnlTemps.Location = new System.Drawing.Point(0, 831);
            this.PnlTemps.Name = "PnlTemps";
            this.PnlTemps.Size = new System.Drawing.Size(1445, 64);
            this.PnlTemps.TabIndex = 5;
            // 
            // LblJour
            // 
            this.LblJour.AutoSize = true;
            this.LblJour.Font = new System.Drawing.Font("Kristen ITC", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblJour.Location = new System.Drawing.Point(37, 14);
            this.LblJour.Name = "LblJour";
            this.LblJour.Size = new System.Drawing.Size(89, 40);
            this.LblJour.TabIndex = 4;
            this.LblJour.Text = "Jour";
            // 
            // PnlNb
            // 
            this.PnlNb.BackColor = System.Drawing.Color.Tan;
            this.PnlNb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlNb.Controls.Add(this.LblTexteDechets);
            this.PnlNb.Controls.Add(this.LblNbDechets);
            this.PnlNb.Controls.Add(this.LblArgent);
            this.PnlNb.Controls.Add(this.LblNbAnimaux);
            this.PnlNb.Controls.Add(this.LblTexteAnimaux);
            this.PnlNb.Controls.Add(this.BtnAgence);
            this.PnlNb.Controls.Add(this.PicArgent);
            this.PnlNb.Location = new System.Drawing.Point(1440, 703);
            this.PnlNb.Name = "PnlNb";
            this.PnlNb.Size = new System.Drawing.Size(161, 192);
            this.PnlNb.TabIndex = 5;
            // 
            // LblTexteDechets
            // 
            this.LblTexteDechets.AutoSize = true;
            this.LblTexteDechets.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTexteDechets.Location = new System.Drawing.Point(15, 94);
            this.LblTexteDechets.Name = "LblTexteDechets";
            this.LblTexteDechets.Size = new System.Drawing.Size(87, 23);
            this.LblTexteDechets.TabIndex = 13;
            this.LblTexteDechets.Text = "Déchets: ";
            // 
            // LblNbDechets
            // 
            this.LblNbDechets.AutoSize = true;
            this.LblNbDechets.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNbDechets.Location = new System.Drawing.Point(103, 94);
            this.LblNbDechets.Name = "LblNbDechets";
            this.LblNbDechets.Size = new System.Drawing.Size(20, 23);
            this.LblNbDechets.TabIndex = 12;
            this.LblNbDechets.Text = "0";
            // 
            // LblArgent
            // 
            this.LblArgent.AutoSize = true;
            this.LblArgent.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblArgent.Location = new System.Drawing.Point(94, 27);
            this.LblArgent.Name = "LblArgent";
            this.LblArgent.Size = new System.Drawing.Size(37, 23);
            this.LblArgent.TabIndex = 11;
            this.LblArgent.Text = "100";
            // 
            // LblNbAnimaux
            // 
            this.LblNbAnimaux.AutoSize = true;
            this.LblNbAnimaux.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNbAnimaux.Location = new System.Drawing.Point(103, 61);
            this.LblNbAnimaux.Name = "LblNbAnimaux";
            this.LblNbAnimaux.Size = new System.Drawing.Size(20, 23);
            this.LblNbAnimaux.TabIndex = 10;
            this.LblNbAnimaux.Text = "0";
            // 
            // LblTexteAnimaux
            // 
            this.LblTexteAnimaux.AutoSize = true;
            this.LblTexteAnimaux.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTexteAnimaux.Location = new System.Drawing.Point(15, 61);
            this.LblTexteAnimaux.Name = "LblTexteAnimaux";
            this.LblTexteAnimaux.Size = new System.Drawing.Size(84, 23);
            this.LblTexteAnimaux.TabIndex = 9;
            this.LblTexteAnimaux.Text = "Animaux:";
            // 
            // BtnAgence
            // 
            this.BtnAgence.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnAgence.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAgence.Location = new System.Drawing.Point(10, 125);
            this.BtnAgence.Name = "BtnAgence";
            this.BtnAgence.Size = new System.Drawing.Size(141, 56);
            this.BtnAgence.TabIndex = 6;
            this.BtnAgence.Text = "Engager Concierge";
            this.BtnAgence.UseVisualStyleBackColor = false;
            this.BtnAgence.Click += new System.EventHandler(this.BtnAgence_Click);
            // 
            // PicArgent
            // 
            this.PicArgent.Image = ((System.Drawing.Image)(resources.GetObject("PicArgent.Image")));
            this.PicArgent.Location = new System.Drawing.Point(30, 12);
            this.PicArgent.Name = "PicArgent";
            this.PicArgent.Size = new System.Drawing.Size(42, 38);
            this.PicArgent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicArgent.TabIndex = 5;
            this.PicArgent.TabStop = false;
            // 
            // PnlInfo
            // 
            this.PnlInfo.BackColor = System.Drawing.Color.AntiqueWhite;
            this.PnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlInfo.Controls.Add(this.LblTitreBoite);
            this.PnlInfo.Location = new System.Drawing.Point(1440, 670);
            this.PnlInfo.Name = "PnlInfo";
            this.PnlInfo.Size = new System.Drawing.Size(161, 33);
            this.PnlInfo.TabIndex = 8;
            // 
            // LblTitreBoite
            // 
            this.LblTitreBoite.AutoSize = true;
            this.LblTitreBoite.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTitreBoite.Location = new System.Drawing.Point(8, 1);
            this.LblTitreBoite.Name = "LblTitreBoite";
            this.LblTitreBoite.Size = new System.Drawing.Size(143, 27);
            this.LblTitreBoite.TabIndex = 6;
            this.LblTitreBoite.Text = "Informations";
            // 
            // TmrTemps
            // 
            this.TmrTemps.Enabled = true;
            this.TmrTemps.Interval = 805;
            this.TmrTemps.Tick += new System.EventHandler(this.TmrTemps_Tick);
            // 
            // TmrRealTime
            // 
            this.TmrRealTime.Interval = 60000;
            this.TmrRealTime.Tick += new System.EventHandler(this.TmrRealTime_Tick);
            // 
            // MondeJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PnlInfo);
            this.Controls.Add(this.PnlNb);
            this.Controls.Add(this.PnlTemps);
            this.Name = "MondeJeu";
            this.Size = new System.Drawing.Size(1602, 898);
            this.Load += new System.EventHandler(this.MondeJeu_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MondeJeu_Paint);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MondeJeu_MouseClick);
            this.PnlTemps.ResumeLayout(false);
            this.PnlTemps.PerformLayout();
            this.PnlNb.ResumeLayout(false);
            this.PnlNb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicArgent)).EndInit();
            this.PnlInfo.ResumeLayout(false);
            this.PnlInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PnlTemps;
        private System.Windows.Forms.Label LblJour;
        private System.Windows.Forms.Panel PnlNb;
        private System.Windows.Forms.Label LblTexteDechets;
        private System.Windows.Forms.Label LblNbDechets;
        private System.Windows.Forms.Label LblArgent;
        private System.Windows.Forms.Label LblNbAnimaux;
        private System.Windows.Forms.Label LblTexteAnimaux;
        private System.Windows.Forms.Button BtnAgence;
        private System.Windows.Forms.PictureBox PicArgent;
        private System.Windows.Forms.Panel PnlInfo;
        private System.Windows.Forms.Label LblTitreBoite;
        private System.Windows.Forms.Timer TmrTemps;
        private System.Windows.Forms.Timer TmrRealTime;
    }
}
