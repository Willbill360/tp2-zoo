﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Humains;
using Animaux;
using Utilitaires;
using PathFinding;
using System.Threading;
using TP2_Zoo;

namespace Monde {
    public partial class MondeJeu : UserControl {
        private Random Rand = new Random();
        private const double PrixEntree = 2;
        private const double PrixNourriture = 1;
        private const double PrixSelonTemps = 1;
        private const double FraisConcierge = 2;
        private const double FraisDechet = 0.10;
        private const double Contravention = 2;
        private const int LargeurTab = 26;
        private const int LongueurTab = 50;
        private const int MAX_ANIMALS_PER_ENCLO = 10;

        private bool ShowGridOverlay = false;
        private bool ShowAIPathOverlay = false;

        private List<Visiteur> Visiteurs;
        private List<Concierge> Concierges;

        private Map Monde;

        DateTime JourActuel = DateTime.Now;
        public double MontantTotalArgent = 100;
        private int NbDechets = 0;
        private int CompteurAnimal = 0;
        private List<int> ListeCompteurJourFaim;

        /// <summary>
        /// Constructeur du user control du MondeJeu
        /// </summary>
        public MondeJeu() {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.BackColor = Color.Transparent;
            DoubleBuffered = true;
            Monde = new Map(50, 26);

            // TODO: Switch those in Map
            Visiteurs = new List<Visiteur>();
            Concierges = new List<Concierge>();
            ListeCompteurJourFaim = new List<int>();
        }

        /// <summary>
        /// Permet de fixer le refresh rate
        /// </summary>
        protected override CreateParams CreateParams {
            get {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        /// <summary>
        /// Fonction appeler lors de la fin du loading du composant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MondeJeu_Load(object sender, EventArgs e) {
            TmrTemps.Start();
            TmrRealTime.Start();
            LblJour.Text = JourActuel.ToLongDateString();
            DemarrerJeu();
        }

        /// <summary>
        /// Permet de "toggle" les overlays
        /// </summary>
        /// <param name="overlay">Le type d'overlay</param>
        internal void ToggleOverlay(string overlay) {
            switch (overlay) {
                case "Grid":
                    ShowGridOverlay = !ShowGridOverlay;
                    break;
                case "AIPath":
                    ShowAIPathOverlay = !ShowAIPathOverlay;
                    break;
            }
        }

        /// <summary>
        /// Permet de démarrer le Thread Principale du jeu
        /// </summary>
        public void DemarrerJeu() {
            Thread thread = new Thread(new ThreadStart(this.BoucleDeJeu));
            thread.IsBackground = true;
            thread.Name = "Boucle de jeu";
            thread.Start();
        }

        /// <summary>
        /// Le Thread principale du jeu
        /// </summary>
        private void BoucleDeJeu() {
            while (true) {
                if (!IsHandleCreated)
                    continue;

                this.BeginInvoke((MethodInvoker)delegate () {
                    if (Visiteurs.Count < GetTotalAnimalCount()) {
                        TuilesHumains typeHumain = (TuilesHumains)Rand.Next((int)TuilesHumains.HUMAIN_1, (int)TuilesHumains.JOUEUR);
                        SpawnVisiteur(typeHumain);
                    }
                    foreach (var visiteur in Visiteurs.ToList()) {
                        visiteur.TimeAlive += 0.5f;
                        if (visiteur.Position == Monde.ExitPoint && visiteur.TimeAlive >= 60) {
                            Visiteurs.Remove(visiteur);
                            Monde.TabHumains[visiteur.Position.X, visiteur.Position.Y] = null;
                        } else {
                            visiteur.Move(Monde);
                            UpdateLabelDechet();
                        }
                    }
                    foreach (var concierge in Concierges) {
                        concierge.Move(Monde);
                    }
                    foreach (var enclo in Monde.ListeEnclo) {
                        foreach (var animal in enclo.ListeAnimaux) {
                            animal.Move(Monde);
                            animal.DeterminerPossibiliteEnceinte();
                        }
                    }
                    
                    Refresh();
                });
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Permet de mettre a jour le nombre de dechet
        /// </summary>
        private void UpdateLabelDechet() {
            int dechetCount = 0;
            for (int x = 0; x < Monde.TuilesDechets.GetLength(0); x++) {
                for (int y = 0; y < Monde.TuilesDechets.GetLength(1); y++) {
                    if (Monde.TuilesDechets[x, y] != -1) {
                        dechetCount++;
                    }
                }
            }
            NbDechets = dechetCount;
            LblNbDechets.Text = dechetCount.ToString();
        }

        /// <summary>
        /// Permet de faire apparaitre un nouveau visiteur
        /// </summary>
        /// <param name="tuileVisiteur">Le type (skin) d'humain a faire apparaitre</param>
        private void SpawnVisiteur(TuilesHumains tuileVisiteur) {
            Visiteur visiteur = new Visiteur(Monde.SpawnPoint, tuileVisiteur);
            Visiteurs.Add(visiteur);
            Monde.TabHumains[visiteur.Position.X, visiteur.Position.Y] = visiteur;
            PayerPrixEntree();
        }

        /// <summary>
        /// Permet au visiteur de payer le prix d'entré au zoo
        /// </summary>
        private void PayerPrixEntree() {
            double prix = 0;

            prix = PrixEntree * GetTotalAnimalCount();
            MontantTotalArgent += prix;
            LblArgent.Text = MontantTotalArgent.ToString();
        }

        /// <summary>
        /// Permet de faire apparaitre un nouveau concierge
        /// </summary>
        private void SpawnConcierge() {
            Concierge concierge = new Concierge(new MapPoint(Monde.SpawnPoint.X + 1, Monde.SpawnPoint.Y));
            Concierges.Add(concierge);
            Monde.TabHumains[concierge.Position.X, concierge.Position.Y] = concierge;
        }
        
        /// <summary>
        /// Permet de peindre le monde
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MondeJeu_Paint(object sender, PaintEventArgs e) {
            int locationX = 0;
            int locationY = 0;

            Graphics g = e.Graphics;

            for (int x = 0; x < Monde.TuilesMonde.GetLength(0); x++) {
                for (int y = 0; y < Monde.TuilesMonde.GetLength(1); y++) {
                    g.DrawImage(TilesetImageGenerator.GetTile(Monde.TuilesMonde[x, y]), locationX, locationY, 32, 32);
                    locationY += 32;
                }
                locationX += 32;
                locationY = 0;
            }
            
            locationX = 0;

            for (int x = 0; x < Monde.TuilesObjets.GetLength(0); x++) {
                for (int y = 0; y < Monde.TuilesObjets.GetLength(1); y++) {
                    if (Monde.TuilesObjets[x, y] != -1) {
                        g.DrawImage(TilesetImageGenerator.GetTile(Monde.TuilesObjets[x, y]), locationX, locationY, 32, 32);
                    }
                    locationY += 32;
                }
                locationX += 32;
                locationY = 0;
            }

            locationX = 0;

            for (int x = 0; x < Monde.TuilesDechets.GetLength(0); x++) {
                for (int y = 0; y < Monde.TuilesDechets.GetLength(1); y++) {
                    if (Monde.TuilesDechets[x, y] != -1) {
                        if(ShowGridOverlay) {
                            g.FillRectangle(Brushes.Purple, locationX, locationY, 32, 32);
                        }
                        g.DrawImage(TilesetImageGenerator.GetTile(Monde.TuilesDechets[x, y]), locationX, locationY, 32, 32);
                    }
                    locationY += 32;
                }
                locationX += 32;
                locationY = 0;
            }

            for (int x = 0; x < Monde.TabHumains.GetLength(0); x++) {
                for (int y = 0; y < Monde.TabHumains.GetLength(1); y++) {
                    if (Monde.TabHumains[x, y] != null) {
                        g.DrawImage(Monde.TabHumains[x, y].OrientationCourrante, x * 32, y * 32, 32, 32);
                    }
                }
            }
            for (int x = 0; x < Monde.TabAnimaux.GetLength(0); x++) {
                for (int y = 0; y < Monde.TabAnimaux.GetLength(1); y++) {
                    if (Monde.TabAnimaux[x, y] != null) {
                        g.DrawImage(Monde.TabAnimaux[x, y].OrientationCourrante, x * 32, y * 32, 32, 32);
                    }
                }
            }

            if (ShowAIPathOverlay) {
                foreach (Visiteur visiteur in Visiteurs) {
                    if (visiteur.TargetPoint != null && visiteur.NextPoint != null) {
                        g.FillEllipse(Brushes.Turquoise, visiteur.NextPoint.X * 32 + 8, visiteur.NextPoint.Y * 32 + 8, 16, 16);
                        foreach (MapPoint mapPoint in visiteur.PathToTarget) {
                            g.FillEllipse(Brushes.Yellow, mapPoint.X * 32 + 8, mapPoint.Y * 32 + 8, 16, 16);
                        }
                        g.FillEllipse(Brushes.Turquoise, visiteur.TargetPoint.X * 32 + 8, visiteur.TargetPoint.Y * 32 + 8, 16, 16);
                    }
                }
                foreach (Concierge concierge in Concierges) {
                    if (concierge.TargetPoint != null && concierge.NextPoint != null) {
                        g.FillEllipse(Brushes.Aqua, concierge.NextPoint.X * 32 + 8, concierge.NextPoint.Y * 32 + 8, 16, 16);
                        foreach (MapPoint mapPoint in concierge.PathToTarget) {
                            g.FillEllipse(Brushes.Beige, mapPoint.X * 32 + 8, mapPoint.Y * 32 + 8, 16, 16);
                        }
                        g.FillEllipse(Brushes.Aqua, concierge.TargetPoint.X * 32 + 8, concierge.TargetPoint.Y * 32 + 8, 16, 16);
                    }
                }
            }

            if (ShowGridOverlay) {
                locationX = 0;
                Font drawFont = new Font("Arial", 7);
                for (int x = 0; x < Monde.WalkableTilesAI.GetLength(0); x++) {
                    for (int y = 0; y < Monde.WalkableTilesAI.GetLength(1); y++) {
                        g.DrawString("(" + x + "," + y + ")", drawFont, Brushes.White, locationX, locationY);
                        if (Monde.WalkableTilesAI[x, y]) {
                            g.DrawRectangle(Pens.Blue, locationX, locationY, 31, 31);
                        } else {
                            g.DrawRectangle(Pens.Red, locationX, locationY, 31, 31);
                        }
                        locationY += 32;
                    }
                    locationX += 32;
                    locationY = 0;
                }
            }
        }

        /// <summary>
        /// Permettre le deplacement du joueur sur la map
        /// </summary>
        /// <param name="direction"></param>
        public void DeplacerJoueur(Direction direction) {
            Monde.Player.Move(Monde, direction);
            this.Refresh();
        }

        /// <summary>
        /// Lorsque l'utilisateur clique sur la map
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MondeJeu_MouseClick(object sender, MouseEventArgs e) {
            int locationX = e.X / 32;
            int locationY = e.Y / 32;
            int position = Monde.Player.Position.X;
            int positionY = Monde.Player.Position.Y;
            
            switch (e.Button) {
                case MouseButtons.Left:
                    //zone de détectiontion
                    if ((position - locationX) < 2 && (position - locationX) > -2) {
                        if ((positionY - locationY) < 2 && (positionY - locationY) > -2) {
                            TuilesAdjacentHero(locationX, locationY);
                        }
                    }
                    break;

                case MouseButtons.Right:
                    //detecte presence d'un animal
                    if (Monde.TabAnimaux[locationX, locationY] != null) {
                        InformationAnimal info = new InformationAnimal();
                        info.RemplirInformationsAnimal(Monde.TabAnimaux[locationX, locationY], GetAllAnimals());
                        this.Controls.Add(info);
                    }

                    //detecte presence d'un visiteur
                    else if (Monde.TabHumains[locationX, locationY] != null) {
                        foreach (Visiteur visiteur in Visiteurs) {
                            if (visiteur.Position.X == locationX && visiteur.Position.Y == locationY) {
                                InformationsHumain info = new InformationsHumain();
                                info.RemplirInformationsVisiteur(visiteur);
                                this.Controls.Add(info);
                            }
                        }
                    }
                    break;
            }
            Refresh();
        }

        /// <summary>
        /// Permet de récupéré tous les animaux de tous les enclos
        /// </summary>
        /// <returns>La liste de tous les animaux</returns>
        private List<Animal> GetAllAnimals() {
            List<Animal> allAnimals = new List<Animal>();

            foreach (Enclo enclo in Monde.ListeEnclo) {
                allAnimals.AddRange(enclo.ListeAnimaux);
            }

            return allAnimals;
        }

        /// <summary>
        /// Activité lorsqu'un case est adjacente au hero
        /// </summary>
        /// <param name="locationX">La position en X</param>
        /// <param name="locationY">La position en Y</param>
        private void TuilesAdjacentHero(int locationX, int locationY) {
            bool clickRien = false;

            if (Monde.TuilesDechets[locationX, locationY] == (int)TuilesDechet.BOUTEILLE || Monde.TuilesDechets[locationX, locationY] == (int)TuilesDechet.GOBELET) {
                Monde.TuilesDechets[locationX, locationY] = -1;
                MapPoint positionDechet = new MapPoint(locationX, locationY);
                for (int i = 0; i < Monde.ListeDechets.Count; i++) {
                    MapPoint currentDechet = Monde.ListeDechets.Dequeue();
                    if (currentDechet != positionDechet) {
                        Monde.ListeDechets.Enqueue(currentDechet);
                    }
                }
                UpdateLabelDechet();
            }
            else if (Monde.TabAnimaux[locationX, locationY] != null) {
                System.IO.Stream audioSound = null;
                string animal = Monde.TabAnimaux[locationX, locationY].GetType().Name;
                switch (animal) {
                    case "Licorne":
                        audioSound = TP2_Zoo.Properties.Resources.licorne;
                        break;
                    case "Lion":
                        audioSound = TP2_Zoo.Properties.Resources.lion;
                        break;
                    case "Rhinoceros":
                        audioSound = TP2_Zoo.Properties.Resources.hippopotame;
                        break;
                    case "Mouton":
                        audioSound = TP2_Zoo.Properties.Resources.mouton;
                        break;
                }

                if(audioSound != null) {
                    System.Media.SoundPlayer audio = new System.Media.SoundPlayer(audioSound);
                    audio.Play();
                }

                Monde.TabAnimaux[locationX, locationY].Faim = "Non";
                Monde.TabAnimaux[locationX, locationY].JourFaim = 0;

                int compteur = 0;
                foreach(var animalListe in GetAllAnimals()) {
                    string type = animalListe.GetType().Name;
                    if (animal.Equals(type)) {
                        ListeCompteurJourFaim[compteur] = 0;
                    }
                    compteur++;
                }

                MontantTotalArgent -= PrixNourriture;
                LblArgent.Text = MontantTotalArgent.ToString();
            }
            else {
                foreach (Enclo enclo in Monde.ListeEnclo) {
                    if (enclo.ClickEstDansEnclo(locationX, locationY)) {
                        clickRien = false;

                        if (enclo.ListeAnimaux.Count < MAX_ANIMALS_PER_ENCLO) {
                            FrmMagasin magasin = new FrmMagasin(this);
                            var type = magasin.ShowDialog();
                            string animalChoisi = magasin.AnimalChoisi;

                            if (enclo.AnimalType == null || enclo.AnimalType == magasin.AnimalChoisi) {
                                double montant = magasin.Montant;
                                if(DiminuerArgentAchatAnimal(magasin.Montant)) {
                                    enclo.AjouterAnimal(Monde, magasin.AnimalChoisi);
                                }
                                break;
                            }
                            else {
                                MessageBox.Show("Vous avez déjà un autre type d'animal dans l'enclo.");
                            }
                        }
                        else {
                            MessageBox.Show("Vous avez déjà trop d'animaux dans cet enclo.");
                        }
                        break;
                    }
                    else {
                        clickRien = true;
                    }
                }

                if (clickRien) {
                    DialogResult dialogResult = MessageBox.Show("Voulez-vous engager un concierge", "Agence d'embauche", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes) {
                        SpawnConcierge();
                    }
                }
            }
        }

        /// <summary>
        /// Diminu le montant d'argent total de l'hero selon le type d'animal
        /// </summary>
        /// <param name="montant">Montant d'argent à payer</param>
        private bool DiminuerArgentAchatAnimal(double montant) {
            if(MontantTotalArgent < montant) {
                MessageBox.Show("Vous n'avez pas assez d'argent ");
                return false;
            }

            MontantTotalArgent = MontantTotalArgent - montant;
            LblArgent.Text = MontantTotalArgent.ToString();
            LblNbAnimaux.Text = GetTotalAnimalCount().ToString();
            return true;
        }

        /// <summary>
        /// Permet d'avoir le compte total des animaux
        /// </summary>
        /// <returns>Le nombre total des animaux</returns>
        private int GetTotalAnimalCount() {
            int animalCount = 0;
            foreach (var enclo in Monde.ListeEnclo) {
                animalCount += enclo.ListeAnimaux.Count;
            }

            return animalCount;
        }

        /// <summary>
        /// Thread secondaire du temps fictif du temps (1 jour à chaque 805ms)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TmrTemps_Tick(object sender, EventArgs e) {
            JourActuel = JourActuel.AddDays(1);
            LblJour.Text = JourActuel.ToLongDateString();

            foreach (var visiteur in Visiteurs) {
                visiteur.AjouterJour();
            }

            foreach (var enclo in Monde.ListeEnclo) {
                foreach (var animal in enclo.ListeAnimaux.ToList()) {
                    if (animal.IsPregnant && animal.GestatingTime >= animal.GestationPriode) {
                        animal.Accouche(Monde);
                    }
                    animal.AjouterJour();
                }
            }

            LblNbAnimaux.Text = GetTotalAnimalCount().ToString();
        }

        /// <summary>
        /// Thread terciaire qui roule a chaque réelle minute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TmrRealTime_Tick(object sender, EventArgs e) {
            double montantPayerVisiteur = 0;
            int caseListe = 0;
            AugmenterCompteur();
            foreach (var animal in GetAllAnimals()) {
                string type = animal.GetType().Name;
                switch (type) {
                    case "Licorne":
                        Licorne lic = (Licorne)animal;
                        lic.Faim = "Oui";
                        lic.JourFaim++;

                        if (ListeCompteurJourFaim[caseListe] == 3) {
                            ListeCompteurJourFaim[caseListe] = 0;
                            PayerContravention(lic);
                        }

                        break;
                    case "Lion":
                        Lion lion = (Lion)animal;
                        lion.Faim = "Oui";
                        lion.JourFaim++;

                        if (ListeCompteurJourFaim[caseListe] == 2) {
                            ListeCompteurJourFaim[caseListe] = 0;
                            PayerContravention(lion);
                        }

                        break;
                    case "Rhinoceros":
                        Rhinoceros rhino = (Rhinoceros)animal;
                        rhino.Faim = "Oui";
                        rhino.JourFaim++;

                        if (ListeCompteurJourFaim[caseListe] == 3) {
                            ListeCompteurJourFaim[caseListe] = 0;
                            PayerContravention(rhino);
                        }

                        break;
                    case "Mouton":
                        Mouton mouton = (Mouton)animal;
                        mouton.Faim = "Oui";
                        mouton.JourFaim++;

                        if (ListeCompteurJourFaim[caseListe] == 2) {
                            ListeCompteurJourFaim[caseListe] = 0;
                            PayerContravention(mouton);
                        }

                        break;
                    default:
                        break;
                }
                LblArgent.Text = MontantTotalArgent.ToString();
                caseListe++;
            } 
            foreach (var visiteur in Visiteurs) {
                if(CompteurAnimal > 0) {
                    montantPayerVisiteur += PrixSelonTemps * CompteurAnimal;
                    if(NbDechets > 0) {
                        montantPayerVisiteur = montantPayerVisiteur - (FraisDechet * NbDechets);
                    }
                }
                else {
                    break;
                }
            }

            foreach (var concierge in Concierges) {
                montantPayerVisiteur -= FraisConcierge;
            }
            MontantTotalArgent += montantPayerVisiteur;
            LblArgent.Text = MontantTotalArgent.ToString();
        }
        
        /// <summary>
        /// Augmente le compteur de minutes de tous les animaux avant qu'ils ont faim
        /// </summary>
        private void AugmenterCompteur() {
            for (int i = 0; i < ListeCompteurJourFaim.Count; i++) {
                ListeCompteurJourFaim[i]++;
            }
        }

        /// <summary>
        /// Payer la contravention
        /// </summary>
        private void PayerContravention(Animal animal) {
            animal.Faim = "Non";
            animal.JourFaim = 0;

            MontantTotalArgent = MontantTotalArgent - Contravention;
        }

        /// <summary>
        /// Permet d'acheter un nouveau concierge via le menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAgence_Click(object sender, EventArgs e) {
            SpawnConcierge();
        }
    }
}
