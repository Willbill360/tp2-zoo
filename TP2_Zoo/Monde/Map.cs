﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PathFinding;
using Humains;
using Animaux;
using Utilitaires;

namespace Monde {
    public class Map {
        public int Width { get; private set; }
        public int Height { get; private set; }

        public MapPoint SpawnPoint;
        public MapPoint ExitPoint;

        public bool[,] WalkableTilesAI;
        public int[,] TuilesMonde;
        public int[,] TuilesObjets;
        public int[,] TuilesDechets;
        public Queue<MapPoint> ListeDechets;
        public Humain[,] TabHumains;
        public Animal[,] TabAnimaux;

        public Joueur Player { get; private set; }
        private List<Visiteur> ListeVisiteurs;

        public List<Enclo> ListeEnclo;

        /// <summary>
        /// Constructeur de la classe Map
        /// </summary>
        /// <param name="width">La longueur du monde</param>
        /// <param name="height">La hauteur du monde</param>
        public Map(int width, int height) {
            Width = width;
            Height = height;

            SpawnPoint = new MapPoint(47, 5);
            ExitPoint = new MapPoint(47, 4);

            TuilesMonde = new int[width, height];
            TuilesObjets = new int[width, height];
            TuilesDechets = new int[width, height];

            ListeDechets = new Queue<MapPoint>();

            TabHumains = new Humain[width, height];
            TabAnimaux = new Animal[width, height];

            ListeEnclo = new List<Enclo>();
            ListeVisiteurs = new List<Visiteur>();

            InitialiserTuilesBase();
            AjouterMaison();
            CreationListeEnclos();

            AjouterDecoration();
            BloquerZone();

            ConstructBasicWalkableGrid();

            CreatePlayer();
        }

        /// <summary>
        /// Construit un grille de tuiles sur laquel les IA peuvents marcher
        /// </summary>
        private void ConstructBasicWalkableGrid() {
            WalkableTilesAI = new bool[Width, Height];

            // Fond
            for (int x = 0; x < TuilesMonde.GetLength(0); x++) {
                for (int y = 0; y < TuilesMonde.GetLength(1); y++) {
                    if (TuilesMonde[x, y] != (int)TuilesBase.HERBE) {
                        WalkableTilesAI[x, y] = false;
                    } else {
                        WalkableTilesAI[x, y] = true;
                    }
                }
            }

            // Objets
            for (int x = 0; x < TuilesObjets.GetLength(0); x++) {
                for (int y = 0; y < TuilesObjets.GetLength(1); y++) {
                    if (TuilesObjets[x, y] != -1) {
                        WalkableTilesAI[x, y] = false;
                    }
                }
            }
        }


        /// <summary>
        /// Création du fond de base
        /// </summary>
        private void InitialiserTuilesBase() {
            for (int i = 0; i < Width; i++) {
                for (int j = 0; j < Height; j++) {
                    TuilesObjets[i, j] = -1;
                    TuilesDechets[i, j] = -1;
                }
            }

            for (int x = 0; x < TuilesMonde.GetLength(0); x++) {
                for (int y = 0; y < TuilesMonde.GetLength(1); y++) {
                    if (y < 3) {
                        TuilesMonde[x, y] = (int)TuilesCloture.MUR;
                    } else {
                        TuilesMonde[x, y] = (int)TuilesBase.HERBE;
                    }
                }
            }
        }

        /// <summary>
        /// Création de la maison dans le jeu
        /// </summary>
        private void AjouterMaison() {
            TuilesMonde[45, 4] = (int)TuilesMaison.COTER_MUR_DROIT_BAS;
            TuilesMonde[46, 4] = (int)TuilesMaison.MUR_MAISON_BAS;
            TuilesMonde[47, 4] = (int)TuilesMaison.PORTE_BAS;
            TuilesMonde[48, 4] = (int)TuilesMaison.COTER_MUR_GAUCHE_BAS;
            TuilesMonde[45, 3] = (int)TuilesMaison.COTER_MUR_DROIT_HAUT;
            TuilesMonde[46, 3] = (int)TuilesMaison.MUR_MAISON_HAUT;
            TuilesMonde[47, 3] = (int)TuilesMaison.PORTE_HAUT;
            TuilesMonde[48, 3] = (int)TuilesMaison.COTER_MUR_GAUCHE_HAUT;

            //TOIT
            TuilesMonde[45, 2] = (int)TuilesMaison.TOIT_DROIT_BAS;
            TuilesMonde[46, 2] = (int)TuilesMaison.TOIT_MILIEU_DROIT_BAS;
            TuilesMonde[47, 2] = (int)TuilesMaison.TOIT_MILIEU_GAUCHE_BAS;
            TuilesMonde[48, 2] = (int)TuilesMaison.TOIT_GAUCHE_BAS;
            TuilesMonde[45, 1] = (int)TuilesMaison.TOIT_DROIT_MILIEU;
            TuilesMonde[46, 1] = (int)TuilesMaison.TOIT_MILIEU_DROIT_MILIEU;
            TuilesMonde[47, 1] = (int)TuilesMaison.TOIT_MILIEU_GAUCHE_MILIEU;
            TuilesMonde[48, 1] = (int)TuilesMaison.TOIT_GAUCHE_MILIEU;
            TuilesMonde[45, 0] = (int)TuilesMaison.TOIT_DROIT_HAUT;
            TuilesMonde[46, 0] = (int)TuilesMaison.TOIT_MILIEU_DROIT_HAUT;
            TuilesMonde[47, 0] = (int)TuilesMaison.TOIT_MILIEU_GAUCHE_HAUT;
            TuilesMonde[48, 0] = (int)TuilesMaison.TOIT_GAUCHE_HAUT;
        }

        /// <summary>
        /// Popule la liste des enclos
        /// </summary>
        private void CreationListeEnclos() {
            int longueur = 12;
            int largeur = 7;
            ListeEnclo.Add(new Enclo(new MapPoint(6, 6), largeur, longueur));
            ListeEnclo.Add(new Enclo(new MapPoint(6, 16), largeur, longueur));
            ListeEnclo.Add(new Enclo(new MapPoint(27, 6), largeur, longueur));
            ListeEnclo.Add(new Enclo(new MapPoint(27, 16), largeur, longueur));

            foreach (Enclo enclo in ListeEnclo) {
                InitialiserTuileEnclos(enclo);
                AjouterCloture(enclo);
            }
        }

        /// <summary>
        /// Initialise les tuiles intérieurs d'un enclos
        /// </summary>
        /// <param name="enclo">Enclo à initialiser</param>
        private void InitialiserTuileEnclos(Enclo enclo) {
            int ligne = enclo.Position.X;
            int colonne = enclo.Position.Y;


            for (int x = ligne; x < ligne + enclo.Longueur; x++) {
                for (int y = colonne; y < colonne + enclo.Largeur; y++) {
                    TuilesMonde[x, y] = (int)TuilesBase.TERRE_MILIEU;
                    if (y == colonne + (enclo.Largeur - 1)) {
                        TuilesMonde[x, y] = (int)TuilesBase.TERRE_BAS;
                    }
                    if (y == colonne) {
                        TuilesMonde[x, y] = (int)TuilesBase.TERRE_HAUT;
                    }
                }
            }
        }

        /// <summary>
        /// Ajouter un enclos dans le jeu
        /// </summary>
        /// <param name="ligne"></param>
        /// <param name="colonne"></param>
        private void AjouterCloture(Enclo enclo) {
            int valeurY = enclo.Position.Y;
            int valeurX = enclo.Position.X;

            //fond
            for (int i = 0; i < enclo.Longueur; i++) {
                TuilesObjets[valeurX, valeurY] = (int)TuilesCloture.CLOTURE_DROITE;
                valeurX += 1;
            }

            //coté droit
            for (int j = 0; j < enclo.Largeur; j++) {
                TuilesObjets[valeurX, valeurY] = (int)TuilesCloture.CLOTURE_SIMPLE;
                valeurY += 1;
            }

            //coté gauche
            valeurY = enclo.Position.Y + 1;
            valeurX = enclo.Position.X;
            for (int k = 0; k < (enclo.Largeur - 2); k++) {
                TuilesObjets[valeurX, valeurY] = (int)TuilesCloture.CLOTURE_SIMPLE;
                valeurY += 1;
            }

            //devant
            for (int l = 0; l < enclo.Longueur; l++) {
                TuilesObjets[valeurX, valeurY] = (int)TuilesCloture.CLOTURE_DROITE;
                valeurX += 1;
            }

            //porte
            TuilesObjets[valeurX - 6, valeurY] = (int)TuilesCloture.CLOTURE_PORTE;
        }

        /// <summary>
        /// Ajouter des décorations
        /// </summary>
        private void AjouterDecoration() {
            TuilesObjets[49, 3] = (int)TuilesBase.BUISSON;
            TuilesObjets[49, 4] = (int)TuilesBase.BUISSON;
            TuilesObjets[43, 3] = (int)TuilesMaison.BOITE_FRUIT_ORANGE;
            TuilesObjets[44, 3] = (int)TuilesMaison.BOITE_FRUIT_POMME;
        }

        /// <summary>
        /// Bloque la zone en dessous du UI avec des clôtures
        /// </summary>
        private void BloquerZone() {
            int valeurX = 45;
            int valeurY = 21;
            for (int x = 0; x < 5; x++) {
                for (int y = 0; y < 5; y++) {
                    TuilesObjets[valeurX, valeurY] = (int)TuilesCloture.CLOTURE_DROITE;
                    valeurX++;
                }
                valeurX = 45;
                valeurY++;
            }
        }

        /// <summary>
        /// Crée le joueur
        /// </summary>
        private void CreatePlayer() {
            Player = new Joueur(new MapPoint(SpawnPoint.X - 1, SpawnPoint.Y + 1));
            TabHumains[Player.Position.X, Player.Position.Y] = Player;
        }
    }
}
