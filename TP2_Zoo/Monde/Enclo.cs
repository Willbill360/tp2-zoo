﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PathFinding;
using Utilitaires;
using Animaux;
using Monde;

namespace Monde {
    public class Enclo {
        public int Largeur { get; private set; }
        public int Longueur { get; private set; }
        public MapPoint Position { get; private set; }
        public List<MapPoint> PositionInterieur { get; private set; }
        public List<Animal> ListeAnimaux { get; private set; }
        public string AnimalType { get; private set; }

        /// <summary>
        /// Constructeur de la classe Enclo
        /// </summary>
        /// <param name="position">Position du coin supérieur gauche de l'enclo</param>
        /// <param name="largeurEnclo">Largeur de l'enclo</param>
        /// <param name="longueurEnclo">Longeur de l'enclo</param>
        public Enclo(MapPoint position, int largeurEnclo, int longueurEnclo) {
            Position = position;
            Largeur = largeurEnclo;
            Longueur = longueurEnclo;

            ListeAnimaux = new List<Animal>();
            AnimalType = null;

            PositionInterieur = new List<MapPoint>();

            for (int x = Position.X + 1; x < (Position.X + longueurEnclo) - 1; x++) {
                for (int y = Position.Y + 1; y < (Position.Y + largeurEnclo) - 1; y++) {
                    PositionInterieur.Add(new MapPoint(x, y));
                }
            }
        }

        /// <summary>
        /// Permet de récupérer les tuiles du monde sur lesquels on peut marcher (dans l'enclo)
        /// </summary>
        /// <param name="monde">Le monde dans lequel on pourra marcher</param>
        /// <returns>Un tableau des tuiles marchables</returns>
        public bool[,] WalkableTilesAI(Map monde) {
            bool[,] walkableTilesAI = new bool[monde.Width, monde.Height];
            for (int x = Position.X + 1; x < (Position.X + Longueur) - 1; x++) {
                for (int y = Position.Y + 1; y < (Position.Y + Largeur) - 1; y++) {
                    walkableTilesAI[x, y] = true;
                }
            }

            return walkableTilesAI;
        }

        /// <summary>
        /// Permet de vérifier si un click se trouve dans l'enceinte de l'enclos
        /// </summary>
        /// <param name="locationX">La position en X du click</param>
        /// <param name="locationY">La position en Y du click</param>
        /// <returns></returns>
        public bool ClickEstDansEnclo(int locationX, int locationY) {
            if (locationX >= Position.X && locationX <= (Position.X + Longueur)) {
                if (locationY >= Position.Y && locationY <= (Position.Y + Largeur - 1)) {
                    if (locationX > Position.X && locationY > Position.Y && locationX < (Position.X + Longueur) && locationY < (Position.Y + Largeur - 1)) {
                        return false;
                    }
                    else {
                        return true;
                    }

                }
            }
            return false;
        }

        /// <summary>
        /// Permet de récupéré la première position disponible pour faire aparaître un nouvel animal
        /// </summary>
        /// <param name="monde">Le monde dans lequel la position se trouve</param>
        /// <returns>Une position pour faire apparaitre un nouvel animal</returns>
        private MapPoint DeterminerSpawnpoint(Map monde) {
            foreach (MapPoint position in PositionInterieur) {
                if(monde.TabAnimaux[position.X, position.Y] == null) {
                    return new MapPoint(position.X, position.Y);
                }
            }

            return null;
        }

        /// <summary>
        /// Permet d'ajouter un animal à l'enclo (et donc dans le monde)
        /// </summary>
        /// <param name="monde">Le monde dans lequel on veux ajouté l'animal</param>
        /// <param name="animalType">Le type de l'animal à ajouter</param>
        /// <returns>Si l'animal a été ajouter</returns>
        public bool AjouterAnimal(Map monde, string animalType) {
            if(AnimalType == null) {
                AnimalType = animalType;
            }

            if(AnimalType != animalType) {
                return false;
            }
            if(ListeAnimaux.Count >= 10) {
                return false;
            }
            MapPoint spawnPoint = DeterminerSpawnpoint(monde);
            if(spawnPoint == null) {
                return false;
            }

            Animal animal = null;
            switch (animalType) {
                case "Licorne":
                    animal = new Licorne(spawnPoint, this);
                    break;
                case "Lion":
                    animal = new Lion(spawnPoint, this);
                    break;
                case "Rhinocéros":
                    animal = new Rhinoceros(spawnPoint, this);
                    break;
                case "Mouton":
                    animal = new Mouton(spawnPoint, this);
                    break;
            }

            ListeAnimaux.Add(animal);
            monde.TabAnimaux[spawnPoint.X, spawnPoint.Y] = animal;

            return true;
        }

    }
}
