﻿namespace TP2_Zoo {
    partial class FrmMagasin {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMagasin));
            this.LblTitre = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LblSigne3 = new System.Windows.Forms.Label();
            this.LblSigne4 = new System.Windows.Forms.Label();
            this.LblSigne2 = new System.Windows.Forms.Label();
            this.LblSigne1 = new System.Windows.Forms.Label();
            this.LblPrixRhino = new System.Windows.Forms.Label();
            this.LblPrixMouton = new System.Windows.Forms.Label();
            this.LblPrixLion = new System.Windows.Forms.Label();
            this.LblPrixLicorne = new System.Windows.Forms.Label();
            this.BtnMouton = new System.Windows.Forms.Button();
            this.BtnRhino = new System.Windows.Forms.Button();
            this.BtnLion = new System.Windows.Forms.Button();
            this.BtnLicorne = new System.Windows.Forms.Button();
            this.PicMouton = new System.Windows.Forms.PictureBox();
            this.PicRhino = new System.Windows.Forms.PictureBox();
            this.PicLion = new System.Windows.Forms.PictureBox();
            this.PicLicorne = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicMouton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRhino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLicorne)).BeginInit();
            this.SuspendLayout();
            // 
            // LblTitre
            // 
            this.LblTitre.AutoSize = true;
            this.LblTitre.Font = new System.Drawing.Font("Kristen ITC", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTitre.Location = new System.Drawing.Point(191, 30);
            this.LblTitre.Name = "LblTitre";
            this.LblTitre.Size = new System.Drawing.Size(169, 40);
            this.LblTitre.TabIndex = 0;
            this.LblTitre.Text = "Animalerie";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Tan;
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.LblTitre);
            this.panel1.Location = new System.Drawing.Point(-1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(559, 100);
            this.panel1.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::TP2_Zoo.Properties.Resources.ImgPatteTourne;
            this.pictureBox2.Location = new System.Drawing.Point(401, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(158, 97);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Tan;
            this.pictureBox1.Image = global::TP2_Zoo.Properties.Resources.ImgPatte;
            this.pictureBox1.Location = new System.Drawing.Point(3, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(115, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.PeachPuff;
            this.panel2.Controls.Add(this.LblSigne3);
            this.panel2.Controls.Add(this.LblSigne4);
            this.panel2.Controls.Add(this.LblSigne2);
            this.panel2.Controls.Add(this.LblSigne1);
            this.panel2.Controls.Add(this.LblPrixRhino);
            this.panel2.Controls.Add(this.LblPrixMouton);
            this.panel2.Controls.Add(this.LblPrixLion);
            this.panel2.Controls.Add(this.LblPrixLicorne);
            this.panel2.Controls.Add(this.BtnMouton);
            this.panel2.Controls.Add(this.BtnRhino);
            this.panel2.Controls.Add(this.BtnLion);
            this.panel2.Controls.Add(this.BtnLicorne);
            this.panel2.Controls.Add(this.PicMouton);
            this.panel2.Controls.Add(this.PicRhino);
            this.panel2.Controls.Add(this.PicLion);
            this.panel2.Controls.Add(this.PicLicorne);
            this.panel2.Location = new System.Drawing.Point(-1, 98);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(559, 220);
            this.panel2.TabIndex = 2;
            // 
            // LblSigne3
            // 
            this.LblSigne3.AutoSize = true;
            this.LblSigne3.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSigne3.Location = new System.Drawing.Point(395, 46);
            this.LblSigne3.Name = "LblSigne3";
            this.LblSigne3.Size = new System.Drawing.Size(22, 27);
            this.LblSigne3.TabIndex = 18;
            this.LblSigne3.Text = "$";
            // 
            // LblSigne4
            // 
            this.LblSigne4.AutoSize = true;
            this.LblSigne4.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSigne4.Location = new System.Drawing.Point(394, 144);
            this.LblSigne4.Name = "LblSigne4";
            this.LblSigne4.Size = new System.Drawing.Size(22, 27);
            this.LblSigne4.TabIndex = 17;
            this.LblSigne4.Text = "$";
            // 
            // LblSigne2
            // 
            this.LblSigne2.AutoSize = true;
            this.LblSigne2.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSigne2.Location = new System.Drawing.Point(132, 144);
            this.LblSigne2.Name = "LblSigne2";
            this.LblSigne2.Size = new System.Drawing.Size(22, 27);
            this.LblSigne2.TabIndex = 16;
            this.LblSigne2.Text = "$";
            // 
            // LblSigne1
            // 
            this.LblSigne1.AutoSize = true;
            this.LblSigne1.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSigne1.Location = new System.Drawing.Point(132, 46);
            this.LblSigne1.Name = "LblSigne1";
            this.LblSigne1.Size = new System.Drawing.Size(22, 27);
            this.LblSigne1.TabIndex = 15;
            this.LblSigne1.Text = "$";
            // 
            // LblPrixRhino
            // 
            this.LblPrixRhino.AutoSize = true;
            this.LblPrixRhino.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPrixRhino.Location = new System.Drawing.Point(367, 46);
            this.LblPrixRhino.Name = "LblPrixRhino";
            this.LblPrixRhino.Size = new System.Drawing.Size(34, 27);
            this.LblPrixRhino.TabIndex = 14;
            this.LblPrixRhino.Text = "40";
            // 
            // LblPrixMouton
            // 
            this.LblPrixMouton.AutoSize = true;
            this.LblPrixMouton.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPrixMouton.Location = new System.Drawing.Point(367, 144);
            this.LblPrixMouton.Name = "LblPrixMouton";
            this.LblPrixMouton.Size = new System.Drawing.Size(35, 27);
            this.LblPrixMouton.TabIndex = 13;
            this.LblPrixMouton.Text = "20";
            // 
            // LblPrixLion
            // 
            this.LblPrixLion.AutoSize = true;
            this.LblPrixLion.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPrixLion.Location = new System.Drawing.Point(100, 144);
            this.LblPrixLion.Name = "LblPrixLion";
            this.LblPrixLion.Size = new System.Drawing.Size(32, 27);
            this.LblPrixLion.TabIndex = 12;
            this.LblPrixLion.Text = "35";
            // 
            // LblPrixLicorne
            // 
            this.LblPrixLicorne.AutoSize = true;
            this.LblPrixLicorne.Font = new System.Drawing.Font("Kristen ITC", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPrixLicorne.Location = new System.Drawing.Point(98, 46);
            this.LblPrixLicorne.Name = "LblPrixLicorne";
            this.LblPrixLicorne.Size = new System.Drawing.Size(34, 27);
            this.LblPrixLicorne.TabIndex = 11;
            this.LblPrixLicorne.Text = "50";
            // 
            // BtnMouton
            // 
            this.BtnMouton.Font = new System.Drawing.Font("Kristen ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMouton.Location = new System.Drawing.Point(422, 137);
            this.BtnMouton.Name = "BtnMouton";
            this.BtnMouton.Size = new System.Drawing.Size(104, 45);
            this.BtnMouton.TabIndex = 10;
            this.BtnMouton.Text = "Mouton";
            this.BtnMouton.UseVisualStyleBackColor = true;
            this.BtnMouton.Click += new System.EventHandler(this.BtnAchat_Click);
            // 
            // BtnRhino
            // 
            this.BtnRhino.Font = new System.Drawing.Font("Kristen ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRhino.Location = new System.Drawing.Point(422, 39);
            this.BtnRhino.Name = "BtnRhino";
            this.BtnRhino.Size = new System.Drawing.Size(104, 45);
            this.BtnRhino.TabIndex = 9;
            this.BtnRhino.Text = "Rhinocéros";
            this.BtnRhino.UseVisualStyleBackColor = true;
            this.BtnRhino.Click += new System.EventHandler(this.BtnAchat_Click);
            // 
            // BtnLion
            // 
            this.BtnLion.Font = new System.Drawing.Font("Kristen ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLion.Location = new System.Drawing.Point(160, 137);
            this.BtnLion.Name = "BtnLion";
            this.BtnLion.Size = new System.Drawing.Size(104, 45);
            this.BtnLion.TabIndex = 8;
            this.BtnLion.Text = "Lion";
            this.BtnLion.UseVisualStyleBackColor = true;
            this.BtnLion.Click += new System.EventHandler(this.BtnAchat_Click);
            // 
            // BtnLicorne
            // 
            this.BtnLicorne.Font = new System.Drawing.Font("Kristen ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLicorne.Location = new System.Drawing.Point(160, 39);
            this.BtnLicorne.Name = "BtnLicorne";
            this.BtnLicorne.Size = new System.Drawing.Size(104, 45);
            this.BtnLicorne.TabIndex = 4;
            this.BtnLicorne.Text = "Licorne";
            this.BtnLicorne.UseVisualStyleBackColor = true;
            this.BtnLicorne.Click += new System.EventHandler(this.BtnAchat_Click);
            // 
            // PicMouton
            // 
            this.PicMouton.BackColor = System.Drawing.Color.Tan;
            this.PicMouton.Location = new System.Drawing.Point(292, 127);
            this.PicMouton.Name = "PicMouton";
            this.PicMouton.Size = new System.Drawing.Size(64, 64);
            this.PicMouton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicMouton.TabIndex = 3;
            this.PicMouton.TabStop = false;
            // 
            // PicRhino
            // 
            this.PicRhino.BackColor = System.Drawing.Color.Tan;
            this.PicRhino.Location = new System.Drawing.Point(292, 29);
            this.PicRhino.Name = "PicRhino";
            this.PicRhino.Size = new System.Drawing.Size(64, 64);
            this.PicRhino.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicRhino.TabIndex = 2;
            this.PicRhino.TabStop = false;
            // 
            // PicLion
            // 
            this.PicLion.BackColor = System.Drawing.Color.Tan;
            this.PicLion.Location = new System.Drawing.Point(28, 127);
            this.PicLion.Name = "PicLion";
            this.PicLion.Size = new System.Drawing.Size(64, 64);
            this.PicLion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicLion.TabIndex = 1;
            this.PicLion.TabStop = false;
            // 
            // PicLicorne
            // 
            this.PicLicorne.BackColor = System.Drawing.Color.Tan;
            this.PicLicorne.Location = new System.Drawing.Point(28, 29);
            this.PicLicorne.Name = "PicLicorne";
            this.PicLicorne.Size = new System.Drawing.Size(64, 64);
            this.PicLicorne.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicLicorne.TabIndex = 0;
            this.PicLicorne.TabStop = false;
            // 
            // FrmMagasin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 318);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMagasin";
            this.Text = "Animalerie";
            this.Load += new System.EventHandler(this.FrmMagasin_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicMouton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicRhino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicLicorne)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblTitre;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox PicLicorne;
        private System.Windows.Forms.PictureBox PicMouton;
        private System.Windows.Forms.PictureBox PicRhino;
        private System.Windows.Forms.PictureBox PicLion;
        private System.Windows.Forms.Button BtnLicorne;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button BtnMouton;
        private System.Windows.Forms.Button BtnRhino;
        private System.Windows.Forms.Button BtnLion;
        private System.Windows.Forms.Label LblSigne3;
        private System.Windows.Forms.Label LblSigne4;
        private System.Windows.Forms.Label LblSigne2;
        private System.Windows.Forms.Label LblSigne1;
        private System.Windows.Forms.Label LblPrixRhino;
        private System.Windows.Forms.Label LblPrixMouton;
        private System.Windows.Forms.Label LblPrixLion;
        private System.Windows.Forms.Label LblPrixLicorne;
    }
}