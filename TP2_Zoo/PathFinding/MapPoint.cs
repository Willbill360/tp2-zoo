﻿/**
 * A* Pathfinding, créé par Sebastian Lague et modifié par William Gagnon
 */
namespace PathFinding {
    /**
    * A 2d point on the grid
    */
    public class MapPoint {
        public int X;
        public int Y;

        public MapPoint() {
            X = 0;
            Y = 0;
        }
        public MapPoint(int x, int y) {
            this.X = x;
            this.Y = y;
        }

        public MapPoint(MapPoint b) {
            X = b.X;
            Y = b.Y;
        }

        public override int GetHashCode() {
            return X ^ Y;
        }

        public override bool Equals(System.Object obj) {
            // Unlikely to compare incorrect type so removed for performance
            // if (!(obj.GetType() == typeof(PathFind.MapPoint)))
            //     return false;
            MapPoint p = (MapPoint)obj;

            if (ReferenceEquals(null, p)) {
                return false;
            }

            // Return true if the fields match:
            return (X == p.X) && (Y == p.Y);
        }

        public bool Equals(MapPoint p) {
            if (ReferenceEquals(null, p)) {
                return false;
            }
            // Return true if the fields match:
            return (X == p.X) && (Y == p.Y);
        }

        public static bool operator ==(MapPoint a, MapPoint b) {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b)) {
                return true;
            }
            if (ReferenceEquals(null, a)) {
                return false;
            }
            if (ReferenceEquals(null, b)) {
                return false;
            }
            // Return true if the fields match:
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(MapPoint a, MapPoint b) {
            return !(a == b);
        }

        public MapPoint Set(int iX, int iY) {
            this.X = iX;
            this.Y = iY;
            return this;
        }
    }
}
