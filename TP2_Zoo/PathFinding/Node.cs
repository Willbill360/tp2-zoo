﻿/**
 * A* Pathfinding, créé par Sebastian Lague et modifié par William Gagnon
 */
namespace PathFinding {
    /**
    * A node in the grid map
    */
    public class Node {
        // node starting params
        public bool Walkable;
        public int GridX;
        public int GridY;
        public float Penalty;

        // calculated values while finding path
        public int gCost;
        public int hCost;
        public Node Parent;

        // create the node
        // _price - how much does it cost to pass this tile. less is better, but 0.0f is for non-Walkable.
        // _gridX, _gridY - tile location in grid.
        public Node(float _price, int _gridX, int _gridY) {
            Walkable = _price != 0.0f;
            Penalty = _price;
            GridX = _gridX;
            GridY = _gridY;
        }

        public int fCost {
            get {
                return gCost + hCost;
            }
        }
    }
}