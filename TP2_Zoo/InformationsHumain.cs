﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Humains;
using Utilitaires;

namespace TP2_Zoo {
    public partial class InformationsHumain : UserControl {

        public InformationsHumain() {
            InitializeComponent();
        }

        public void RemplirInformationsVisiteur(Visiteur visiteur) {
            string genre = visiteur.Genre.ToString().ToLower();
            genre = genre.Substring(0, 1).ToUpper() + genre.Substring(1);
            string nom = visiteur.Prenom;

            PicVisiteur.Image = visiteur.Orientations[0];
            nom = nom.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
            LblNomComplet.Text = nom + " " + visiteur.Nom;
            LblGenre.Text = genre;
            LblJours.Text = visiteur.DayInsideZoo + " Jours";
        }

        private void BtnFermer_Click(object sender, EventArgs e) {
            this.Parent.Controls.Remove(this);
        }
    }
}
