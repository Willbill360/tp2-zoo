﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Humains;

namespace Utilitaires {
    class LoadTextRessource {
        public static List<string> LoadPrenom(Sexe sexe) {
            String ressource = "";
            if (sexe == Sexe.HOMME) {
                ressource = TP2_Zoo.Properties.Resources.Prenom_Gars;
            } else if(sexe == Sexe.FEMME) {
                ressource = TP2_Zoo.Properties.Resources.Prenom_Filles;
            }

            var prenomFile = ressource.Split('\n');
            return new List<string>(prenomFile);
        }

        public static List<string> LoadNomFamille() {
            var familleFile = TP2_Zoo.Properties.Resources.NomDeFamille.Split('\n');
            return new List<string>(familleFile);
        }
    }
}
