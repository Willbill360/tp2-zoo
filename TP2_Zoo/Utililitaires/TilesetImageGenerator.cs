﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitaires {
    public enum TuilesCloture {
        CLOTURE_SIMPLE = 0,
        CLOTURE_DROITE,
        CLOTURE_PORTE,
        MUR
    }
    public enum TuilesBase {
        HERBE = 4,
        TERRE_HAUT,
        TERRE_MILIEU,
        TERRE_BAS,
        BUISSON
    }


    public enum TuilesAnimal {
        LICORNE_FACE = 9,
        LICORNE_DOS,
        LION_FACE,
        LION_DOS,
        MOUTON_FACE,
        MOUTON_DOS,
        RHINO_FACE,
        RHINO_DOS
    }

    public enum TuilesDechet {
        BOUTEILLE = 17,
        GOBELET
    }

    public enum TuilesMaison {
        COTER_MUR_DROIT_BAS = 19,
        MUR_MAISON_BAS,
        PORTE_BAS,
        COTER_MUR_GAUCHE_BAS,
        COTER_MUR_DROIT_HAUT,
        MUR_MAISON_HAUT,
        PORTE_HAUT,
        COTER_MUR_GAUCHE_HAUT,

        TOIT_DROIT_BAS,
        TOIT_MILIEU_DROIT_BAS,
        TOIT_MILIEU_GAUCHE_BAS,
        TOIT_GAUCHE_BAS,
        TOIT_DROIT_MILIEU,
        TOIT_MILIEU_DROIT_MILIEU,
        TOIT_MILIEU_GAUCHE_MILIEU,
        TOIT_GAUCHE_MILIEU,
        TOIT_DROIT_HAUT,
        TOIT_MILIEU_DROIT_HAUT,
        TOIT_MILIEU_GAUCHE_HAUT,
        TOIT_GAUCHE_HAUT,

        BOITE_FRUIT_POMME,
        BOITE_FRUIT_ORANGE

    }

    // Type d'humain, increment de 10 puisqu'il Y a 10 poses
    public enum TuilesHumains {
        CONCIERGE = 41,
        /*
        CONCIERGE_FACE_IDLE = 41,
        CONCIERGE_DOS_IDLE,
        CONCIERGE_GAUCHE_IDLE,
        CONCIERGE_FACE_MARCHE,
        CONCIERGE_DOS_MARCHE,
        CONCIERGE_GAUCHE_MARCHE_1,
        CONCIERGE_GAUCHE_MARCHE_2,
        CONCIERGE_DROIT_MARCHE_2,
        CONCIERGE_DROIT_MARCHE_1,
        CONCIERGE_DROIT_IDLE,
        */
        HUMAIN_1 = 51,
        HUMAIN_2 = 61,
        HUMAIN_3 = 71,
        HUMAIN_4 = 81,
        HUMAIN_5 = 91,
        HUMAIN_6 = 101,
        HUMAIN_7 = 111,
        HUMAIN_8 = 121,
        JOUEUR = 131
    }

    class TilesetImageGenerator {
        // Différentes tailles concernant les images dans le fichier de tuiles de jeu
        public const int IMAGE_WIDTH = 32, IMAGE_HEIGHT = 32;
        private const int TILE_LEFT = 0, TILE_TOP = 0;
        private const int SEPARATEUR_TILE = 0;

        private static List<TileCoord> listeCoord = new List<TileCoord>();
        private static List<Bitmap> listeBitmap = new List<Bitmap>();

        /// <summary>
        /// Constructeur statique
        /// </summary>
        static TilesetImageGenerator() {
            Image zooTileset = TP2_Zoo.Properties.Resources.zoo_tileset;
            Image humainTileset = TP2_Zoo.Properties.Resources.personnages;

            GenererCloture(zooTileset);

            GenererSol(zooTileset);

            GenererAnimaux(zooTileset);

            GenererDechets(zooTileset);

            GenererMaison(zooTileset);

            GenererToit(zooTileset);

            GenererHumains(humainTileset);
        }

        private static void GenererHumains(Image source) {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    listeCoord.Add(new TileCoord() { Ligne = i, Colonne = j });
                }
            }

            for (int i = (int)TuilesHumains.CONCIERGE; i < (int)TuilesHumains.JOUEUR + 10; i++) {
                listeBitmap.Add(LoadTile(i, source));
            }
        }

        private static void GenererCloture(Image source) {
            listeCoord.Add(new TileCoord() { Ligne = 14, Colonne = 23 }); // CLOTURE_SIMPLE
            listeCoord.Add(new TileCoord() { Ligne = 14, Colonne = 22 }); // CLOTURE_DROITE
            listeCoord.Add(new TileCoord() { Ligne = 11, Colonne = 18 }); // CLOTURE_PORTE
            listeCoord.Add(new TileCoord() { Ligne = 6, Colonne = 18 }); // MUR

            listeBitmap.Add(LoadTile((int)TuilesCloture.CLOTURE_SIMPLE, source)); // CLOTURE_SIMPLE
            listeBitmap.Add(LoadTile((int)TuilesCloture.CLOTURE_DROITE, source)); // CLOTURE_DROITE
            listeBitmap.Add(LoadTile((int)TuilesCloture.CLOTURE_PORTE, source)); // CLOTURE_PORTE
            listeBitmap.Add(LoadTile((int)TuilesCloture.MUR, source)); // MUR
        }

        private static void GenererSol(Image source) {
            listeCoord.Add(new TileCoord() { Ligne = 9, Colonne = 0 }); // HERBE
            listeCoord.Add(new TileCoord() { Ligne = 1, Colonne = 10 }); // TERRE_HAUT
            listeCoord.Add(new TileCoord() { Ligne = 2, Colonne = 10 }); // TERRE_MILIEU
            listeCoord.Add(new TileCoord() { Ligne = 3, Colonne = 10 }); // TERRE_BAS
            listeCoord.Add(new TileCoord() { Ligne = 0, Colonne = 10 }); // BUISSON

            listeBitmap.Add(LoadTile((int)TuilesBase.HERBE, source)); // HERBE
            listeBitmap.Add(LoadTile((int)TuilesBase.TERRE_HAUT, source)); // TERRE_HAUT
            listeBitmap.Add(LoadTile((int)TuilesBase.TERRE_MILIEU, source)); // MILIEU
            listeBitmap.Add(LoadTile((int)TuilesBase.TERRE_BAS, source)); // TERRE_BAS
            listeBitmap.Add(LoadTile((int)TuilesBase.BUISSON, source)); // BUISSON
        }

        private static void GenererAnimaux(Image source) {
            listeCoord.Add(new TileCoord() { Ligne = 17, Colonne = 18 }); // LICORNE_FACE
            listeCoord.Add(new TileCoord() { Ligne = 16, Colonne = 17 }); // LICORNE_DOS
            listeCoord.Add(new TileCoord() { Ligne = 17, Colonne = 27 }); // LION_FACE
            listeCoord.Add(new TileCoord() { Ligne = 18, Colonne = 28 }); // LION_DOS
            listeCoord.Add(new TileCoord() { Ligne = 20, Colonne = 9 }); // MOUTON_FACE
            listeCoord.Add(new TileCoord() { Ligne = 20, Colonne = 12 }); // MOUTON_DOS
            listeCoord.Add(new TileCoord() { Ligne = 20, Colonne = 16 }); // RHINO_FACE
            listeCoord.Add(new TileCoord() { Ligne = 22, Colonne = 23 }); // RHINO_DOS

            listeBitmap.Add(LoadTile((int)TuilesAnimal.LICORNE_FACE, source)); // LICORNE_FACE
            listeBitmap.Add(LoadTile((int)TuilesAnimal.LICORNE_DOS, source)); // LICORNE_DOS
            listeBitmap.Add(LoadTile((int)TuilesAnimal.LION_FACE, source)); // LION_FACE
            listeBitmap.Add(LoadTile((int)TuilesAnimal.LION_DOS, source)); // LION_DOS
            listeBitmap.Add(LoadTile((int)TuilesAnimal.MOUTON_FACE, source)); // MOUTON_FACE
            listeBitmap.Add(LoadTile((int)TuilesAnimal.MOUTON_DOS, source)); // MOUTON_DOS
            listeBitmap.Add(LoadTile((int)TuilesAnimal.RHINO_FACE, source)); // RHINO_FACE
            listeBitmap.Add(LoadTile((int)TuilesAnimal.RHINO_DOS, source)); // RHINO_DOS
        }

        private static void GenererDechets(Image source) {
            listeCoord.Add(new TileCoord() { Ligne = 23, Colonne = 0 }); // BOUTEILLE
            listeCoord.Add(new TileCoord() { Ligne = 23, Colonne = 1 }); // GOBELET

            listeBitmap.Add(LoadTile((int)TuilesDechet.BOUTEILLE, source)); // BOUTEILLE
            listeBitmap.Add(LoadTile((int)TuilesDechet.GOBELET, source)); // GOBELET
        }

        private static void GenererMaison(Image source) {
            listeCoord.Add(new TileCoord() { Ligne = 8, Colonne = 12 }); // COTER_MUR_DROIT_BAS
            listeCoord.Add(new TileCoord() { Ligne = 8, Colonne = 13 }); // MUR_MAISON_BAS
            listeCoord.Add(new TileCoord() { Ligne = 8, Colonne = 14 }); // PORTE_BAS
            listeCoord.Add(new TileCoord() { Ligne = 8, Colonne = 15 }); // COTER_MUR_GAUCHE_BAS
            listeCoord.Add(new TileCoord() { Ligne = 7, Colonne = 12 }); // COTER_MUR_DROIT_HAUT
            listeCoord.Add(new TileCoord() { Ligne = 7, Colonne = 13 }); // MUR_MAISON_HAUT
            listeCoord.Add(new TileCoord() { Ligne = 7, Colonne = 14 }); // PORTE_HAUT
            listeCoord.Add(new TileCoord() { Ligne = 7, Colonne = 15 }); // COTER_MUR_GAUCHE_BAS

            listeBitmap.Add(LoadTile((int)TuilesMaison.COTER_MUR_DROIT_BAS, source)); // COTER_MUR_DROIT_BAS 
            listeBitmap.Add(LoadTile((int)TuilesMaison.MUR_MAISON_BAS, source)); // MUR_MAISON_BAS
            listeBitmap.Add(LoadTile((int)TuilesMaison.PORTE_BAS, source)); // PORTE_BAS
            listeBitmap.Add(LoadTile((int)TuilesMaison.COTER_MUR_GAUCHE_BAS, source)); // COTER_MUR_GAUCHE_BAS
            listeBitmap.Add(LoadTile((int)TuilesMaison.COTER_MUR_DROIT_HAUT, source)); // COTER_MUR_DROIT_HAUT
            listeBitmap.Add(LoadTile((int)TuilesMaison.MUR_MAISON_HAUT, source)); // MUR_MAISON_HAUT
            listeBitmap.Add(LoadTile((int)TuilesMaison.PORTE_HAUT, source)); // PORTE_HAUT
            listeBitmap.Add(LoadTile((int)TuilesMaison.COTER_MUR_GAUCHE_HAUT, source)); // COTER_MUR_GAUCHE_HAUT
        }

        private static void GenererToit(Image source) {
            listeCoord.Add(new TileCoord() { Ligne = 6, Colonne = 12 }); // TOIT_DROIT_BAS
            listeCoord.Add(new TileCoord() { Ligne = 6, Colonne = 13 }); // TOIT_MILIEU_DROIT_BAS
            listeCoord.Add(new TileCoord() { Ligne = 6, Colonne = 14 }); // TOIT_MILIEU_GAUCHE_BAS
            listeCoord.Add(new TileCoord() { Ligne = 6, Colonne = 15 }); // TOIT_GAUCHE_BAS
            listeCoord.Add(new TileCoord() { Ligne = 5, Colonne = 12 }); // TOIT_DROIT_MILIEU
            listeCoord.Add(new TileCoord() { Ligne = 5, Colonne = 13 }); // TOIT_MILIEU_DROIT_MILIEU
            listeCoord.Add(new TileCoord() { Ligne = 5, Colonne = 14 }); // TOIT_MILIEU_GAUCHE_MILIEU
            listeCoord.Add(new TileCoord() { Ligne = 5, Colonne = 15 }); // TOIT_GAUCHE_MILIEU
            listeCoord.Add(new TileCoord() { Ligne = 4, Colonne = 12 }); // TOIT_DROIT_HAUT
            listeCoord.Add(new TileCoord() { Ligne = 4, Colonne = 13 }); // TOIT_MILIEU_DROIT_HAUT
            listeCoord.Add(new TileCoord() { Ligne = 4, Colonne = 14 }); // TOIT_MILIEU_GAUCHE_HAUT
            listeCoord.Add(new TileCoord() { Ligne = 4, Colonne = 15 }); // TOIT_GAUCHE_HAUT

            listeCoord.Add(new TileCoord() { Ligne = 6, Colonne = 23 }); // BOITE_FRUIT_POMME
            listeCoord.Add(new TileCoord() { Ligne = 6, Colonne = 22 }); // BOITE_FRUIT_ORANGE

            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_DROIT_BAS, source)); // TOIT_DROIT_BAS
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_MILIEU_DROIT_BAS, source)); // TOIT_MILIEU_DROIT_BAS
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_MILIEU_GAUCHE_BAS, source)); // TOIT_MILIEU_GAUCHE_BAS
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_GAUCHE_BAS, source)); // TOIT_GAUCHE_BAS
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_DROIT_MILIEU, source)); // TOIT_DROIT_MILIEU
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_MILIEU_DROIT_MILIEU, source)); // TOIT_MILIEU_DROIT_MILIEU
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_MILIEU_GAUCHE_MILIEU, source)); // TOIT_MILIEU_GAUCHE_MILIEU
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_GAUCHE_MILIEU, source)); // TOIT_GAUCHE_MILIEU
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_DROIT_HAUT, source)); // TOIT_DROIT_HAUT
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_MILIEU_DROIT_HAUT, source)); // TOIT_MILIEU_HAUT
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_MILIEU_GAUCHE_HAUT, source)); // TOIT_GAUCHE_HAUT
            listeBitmap.Add(LoadTile((int)TuilesMaison.TOIT_GAUCHE_HAUT, source)); // TOIT_GAUCHE_HAUT

            listeBitmap.Add(LoadTile((int)TuilesMaison.BOITE_FRUIT_POMME, source)); // BOITE_FRUIT_POMME
            listeBitmap.Add(LoadTile((int)TuilesMaison.BOITE_FRUIT_ORANGE, source)); // BOITE_FRUIT_ORANGE
        }

        private static Bitmap LoadTile(int posListe, Image source) {
            TileCoord coord = listeCoord[posListe];
            Rectangle crop = new Rectangle(TILE_LEFT + (coord.Colonne * (IMAGE_WIDTH + SEPARATEUR_TILE)), TILE_TOP + coord.Ligne * (IMAGE_HEIGHT + SEPARATEUR_TILE), IMAGE_WIDTH, IMAGE_HEIGHT);

            var bmp = new Bitmap(crop.Width, crop.Height);
            using (var gr = Graphics.FromImage(bmp)) {
                gr.DrawImage(source, new Rectangle(0, 0, bmp.Width, bmp.Height), crop, GraphicsUnit.Pixel);
            }
            return bmp;
        }

        public static Bitmap GetTile(int posListe) {
            return listeBitmap[posListe];
        }

    }

    public class TileCoord {
        public int Ligne { get; set; }
        public int Colonne { get; set; }
    }
}

