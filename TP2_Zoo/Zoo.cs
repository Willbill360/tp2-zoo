﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Monde;

namespace TP2_Zoo {
    
    public partial class FrmZoo : Form {
        
        public FrmZoo() {
            this.KeyPreview = true;
            InitializeComponent();

            DoubleBuffered = true;
        }

        protected override CreateParams CreateParams {
            get {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        
        private void FrmZoo_KeyPress(object sender, KeyPressEventArgs e) {
            switch (char.ToUpper(e.KeyChar)) {
                case (char)Keys.W:
                case (char)Keys.Up:
                    MondeJeu.DeplacerJoueur(Humains.Direction.NORD);
                    break;
                case (char)Keys.A:
                case (char)Keys.Left:
                    MondeJeu.DeplacerJoueur(Humains.Direction.OUEST);
                    break;
                case (char)Keys.S:
                case (char)Keys.Down:
                    MondeJeu.DeplacerJoueur(Humains.Direction.SUD);
                    break;
                case (char)Keys.D:
                case (char)Keys.Right:
                    MondeJeu.DeplacerJoueur(Humains.Direction.EST);
                    break;
                case (char)Keys.G:
                    MondeJeu.ToggleOverlay("Grid");
                    break;
                case (char)Keys.H:
                    MondeJeu.ToggleOverlay("AIPath");
                    break;
                default:
                    break;
            }
        }
        

        private void àProposToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Cynthia Cahill" + "\n" + "William Gagnon " + "\n" + "9 Mai 2019");
        }
        
        private void FrmZoo_FormClosing(object sender, FormClosingEventArgs e) {
            double montantTotal = MondeJeu.MontantTotalArgent;
            MessageBox.Show("Vous avez finis la partie avec " + montantTotal + "$");
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e) {
            double montantTotal = MondeJeu.MontantTotalArgent;
            MessageBox.Show("Vous avez finis la partie avec " + montantTotal + "$");
        }
    }
}
